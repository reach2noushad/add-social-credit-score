from nltk.tokenize import word_tokenize, sent_tokenize
import re

from Data_Extractor import ImageSegment


class TextExtract:

    def __init__(self, location):
        self.startMonth = ""
        self.value = ""
        self.location = location
        self.text_array = []
        self.text = ""

    def extractcaller(self, location):
        self.text_array = ImageSegment(location).image_get()
        for i in self.text_array:
            stop = self.textparser(i)
            if stop is True: break

    def textparser(self, text):
        check = ['Meter', 'Reading', 'information', 'Meter', 'Number',
                 'start', 'read', 'end', 'read', 'total', 'consumed', 'units',
                 'used', 'over', 'days']
        tokencounter = 0
        for i in word_tokenize(text):
            if i in check: tokencounter += 1

        if tokencounter > (len(check)/2):
            self.text = text
            return True
        else: return False

    def handler(self):
        self.extractcaller(self.location)

        # noinspection PyPep8Naming
        def isInt(proc):
            try:
                int(proc)
                return True
            except():
                return False
        try:
            if len(self.text) != 0:
                print(self.text)
                self.text = re.sub('\n\n', '\n', self.text)
                for i in self.text.split('\n'):
                    if len(i) != 0:
                        use = word_tokenize(i)
                        if use[0][-1] == 't' and use[1] == 'read': self.startMonth = use[2]
                        elif use[1].lower() == 'kwh' and isInt(use[0]): self.value = use[0]
            else:
                return False, "", ""
            if len(self.startMonth) != 0 and len(self.value) != 0: return True, self.startMonth, self.value
            else: False, "", ""
        except:
            return False, "", ""


# add on a check for the last sentence with a ner









if __name__ == "__main__":
    print(TextExtract("/home/sabari/Workspace/other_works/pdf-read/test.JPEG").handler())