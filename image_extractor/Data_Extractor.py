import cv2
import imutils as imutils
import numpy as np
import pytesseract


class ImageSegment:

    def __init__(self, location):
        self.location = location

    @staticmethod
    def crop(numpy_image, res):
        left = res[0]
        top = res[1]
        right = res[2]
        bottom = res[3]
        return numpy_image[top:bottom, left:right]

    def image_get(self):
        # print(pytesseract.image_to_string(self.location))
        # noinspection SpellCheckingInspection
        originalimage = cv2.imread(self.location)
        image = cv2.cvtColor(originalimage, cv2.COLOR_BGR2GRAY)
        edges = cv2.Canny(image, 200, 150)
        kernel = np.ones((30, 30), np.uint8)
        img_dilation = cv2.dilate(edges, kernel, iterations=1)
        cnts = cv2.findContours(img_dilation.copy(), cv2.RETR_EXTERNAL,
                                cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        cou = 0

        text_array = []

        for c in cnts:
            print(cou)
            cou += 1
            (x, y, w, h) = cv2.boundingRect(c)
            cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
            img = self.crop(originalimage, [x+18, y+18, x+w+18, y+h+18])
            text = pytesseract.image_to_string(img)
            text_array.append(text)

        print(text_array)
        return text_array
