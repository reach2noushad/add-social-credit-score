from flask import Flask,request,jsonify
import os

from Text_Checker import TextExtract

app = Flask(__name__)


@app.route('/image', methods=['POST'])
def imageread():
    # if request.method == 'POST':
    try:
        data = request.files['image']
        target = "./image"

        if not os.path.exists(target):
            os.makedirs(target)

        filename = 'image.jpg'
        image_path = os.path.join(target, filename)
        data.save(image_path)

        check, month, value = TextExtract(image_path).handler()

        if check is True:
            return jsonify({'month': month, 'value': value})
        else: return "Invalid Image"

        #do the check for the last line and give a combined output of the two

    except:
        return "Invalid Image"


app.run(debug=True, host="0.0.0.0", port="2123")
