package adda.ae.com.addademo.stepcounter;

public interface StepListener {
    public void step(long timeNs);
}
