package adda.ae.com.addademo.models;

public class HomeScoreListItemModel {


    public  HomeScoreListItemModel(String description, String value, boolean isSection)
    {
        this.description = description;
        this.value = value;
        this.isSection = isSection;
    }


    public String description;
    public boolean isSection;
    public  String value;

}
