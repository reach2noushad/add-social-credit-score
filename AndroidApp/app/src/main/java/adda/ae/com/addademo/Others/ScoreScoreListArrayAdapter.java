package adda.ae.com.addademo.Others;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import adda.ae.com.addademo.R;
import adda.ae.com.addademo.models.ScoreScoreListItemModel;

public class ScoreScoreListArrayAdapter extends ArrayAdapter {


    Context context;
    ArrayList<ScoreScoreListItemModel> itemModel;
    int resourceId;
    int sectionResourceId;

    public ScoreScoreListArrayAdapter(Context applicationContext, int resource, int sectionResourceId, ArrayList<ScoreScoreListItemModel> text){
        super(applicationContext, resource);
        this.context=applicationContext;
        this.itemModel=text;
        this.resourceId = resource;
        this.sectionResourceId = sectionResourceId;
    }
    @Override
    public int getCount() {
        return itemModel.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View listrow= null;
        ScoreScoreListItemModel model = itemModel.get(position);
        if(model.isSection)
        {
            listrow = inflater.inflate(this.sectionResourceId,null);//R.layout.home_score_list_item
            TextView textView = (TextView) listrow.findViewById(R.id.description);
            textView.setText(model.header);
        }else{

            listrow = inflater.inflate(this.resourceId,null);//R.layout.home_score_list_item

            TextView textView = (TextView) listrow.findViewById(R.id.description);
            TextView scoreTextView = (TextView) listrow.findViewById(R.id.scorevalue);

            TextView firstDescriptionTextView = (TextView) listrow.findViewById(R.id.firstdescription);
            TextView firstValueTextView = (TextView) listrow.findViewById(R.id.firstvalue);

            LinearLayout additionalInfoLayout = (LinearLayout)listrow.findViewById(R.id.additionalInfoLayout);

            TextView secondDescriptionTextView = (TextView) listrow.findViewById(R.id.seconddescription);
            TextView secondValueTextView = (TextView) listrow.findViewById(R.id.secondValue);

            TextView thirdDescriptionTextView = (TextView) listrow.findViewById(R.id.thirddescription);
            TextView thirdValueTextView = (TextView) listrow.findViewById(R.id.thirdValue);


            switch (model.type)
            {
                case Steps:
                    textView.setText(model.header);

                    firstDescriptionTextView.setText(model.firstDescription);
                    firstDescriptionTextView.setVisibility(View.VISIBLE);

                    firstValueTextView.setText(model.firstValue);
                    firstValueTextView.setVisibility(View.VISIBLE);

                    additionalInfoLayout.setVisibility(View.GONE);

                    scoreTextView.setText(model.score);
                    break;

                case Transportation:
                    textView.setText(model.header);

                    firstDescriptionTextView.setText(model.firstDescription);
                    firstDescriptionTextView.setVisibility(View.VISIBLE);

                    firstValueTextView.setText(model.firstValue);
                    firstValueTextView.setVisibility(View.VISIBLE);

                    additionalInfoLayout.setVisibility(View.VISIBLE);


                    secondDescriptionTextView.setText(model.secondDescription);
                    secondDescriptionTextView.setVisibility(View.VISIBLE);

                    secondValueTextView.setText(model.secondValue);
                    secondValueTextView.setVisibility(View.VISIBLE);

                    thirdDescriptionTextView.setText(model.thirdDescription);
                    thirdDescriptionTextView.setVisibility(View.VISIBLE);

                    thirdValueTextView.setText(model.thirdValue);
                    thirdValueTextView.setVisibility(View.VISIBLE);

                    scoreTextView.setText(model.score);
                    break;

                case SocialVolunteer:
                    textView.setText(model.header);

                    firstDescriptionTextView.setText(model.firstDescription);
                    firstDescriptionTextView.setVisibility(View.VISIBLE);

                    firstValueTextView.setText(model.firstValue);
                    firstValueTextView.setVisibility(View.VISIBLE);

                    additionalInfoLayout.setVisibility(View.GONE);

                    scoreTextView.setText(model.score);
                    break;

                case EnergyUse:
                    textView.setText(model.header);

                    firstDescriptionTextView.setText(model.firstDescription);
                    firstDescriptionTextView.setVisibility(View.VISIBLE);

                    firstValueTextView.setText(model.firstValue);
                    firstValueTextView.setVisibility(View.VISIBLE);

                    additionalInfoLayout.setVisibility(View.GONE);

                    scoreTextView.setText(model.score);
                    break;

                case ReportIncident:
                    textView.setText(model.header);

                    firstDescriptionTextView.setText(model.firstDescription);
                    firstDescriptionTextView.setVisibility(View.VISIBLE);

                    firstValueTextView.setText(model.firstValue);
                    firstValueTextView.setVisibility(View.VISIBLE);

                    additionalInfoLayout.setVisibility(View.GONE);

                    scoreTextView.setText(model.score);
                    break;

            }

        }

        return listrow;
    }
}
