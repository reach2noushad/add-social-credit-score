package adda.ae.com.addademo.userinterface;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import adda.ae.com.addademo.R;
import adda.ae.com.addademo.webservice.IAPIServiceResponseListener;
import adda.ae.com.addademo.webservice.IRetrofitResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRegisterActivity extends AppCompatActivity implements View.OnClickListener, IAPIServiceResponseListener, Callback<IRetrofitResponse> {
    Button mRegister;
    private TextInputEditText mName;
    private TextInputEditText mEid;
    private TextInputEditText mPass;
    private TextInputEditText mMob;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_regitration);
        mRegister = findViewById(R.id.btn_register_confirm);
        mRegister.setOnClickListener(this);
        mName = findViewById(R.id.mNameTextInputEditText);
        mEid =  findViewById(R.id.eidTextInputEditText);
        mPass = findViewById(R.id.passTextInputEditText);
        mMob = findViewById(R.id.regMobNumbTextInputEditText);

    }

    @Override
    public void onTaskComplete() {

    }

    @Override
    public void updateUI() {

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_register_confirm)
        {
            //Toast.makeText(this, "login clicked", Toast.LENGTH_SHORT).show();
            if(mName.getText().toString().equalsIgnoreCase("") || (mEid.getText().toString().equalsIgnoreCase("") || (mMob.getText().toString().equalsIgnoreCase("")) || (mPass.getText().toString().equalsIgnoreCase("")))){
                Toast.makeText(this, "Fields should not be empty!", Toast.LENGTH_SHORT).show();
            }else{
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
            }

        }

    }

    @Override
    public void onResponse(Call<IRetrofitResponse> call, Response<IRetrofitResponse> response) {

    }

    @Override
    public void onFailure(Call<IRetrofitResponse> call, Throwable t) {

    }
}
