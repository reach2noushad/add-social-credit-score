package adda.ae.com.addademo.userinterface;


import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import adda.ae.com.addademo.Others.HomeRecyclerViewAdapter;
import adda.ae.com.addademo.Others.HomeScoreListArrayAdapter;
import adda.ae.com.addademo.Others.FeatureType;
import adda.ae.com.addademo.R;
import adda.ae.com.addademo.models.HomeScoreListItemModel;
import adda.ae.com.addademo.models.HomeTile;
import adda.ae.com.addademo.stepcounter.StepDetector;
import adda.ae.com.addademo.stepcounter.StepListener;

public class HomeFragment extends Fragment implements SensorEventListener, StepListener {

    SensorManager sensorManager = null;
    TextView stepsTextView = null;
    Sensor accel;
    StepDetector simpleStepDetector = null;
    private static final String TEXT_NUM_STEPS = "Number of Steps: ";
    private int numSteps;
    private HomeRecyclerViewAdapter tileAdapter;

    private List<HomeTile> tileList = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.home_fragment_layout, viewGroup, false);
//        stepsTextView =  (TextView) view.findViewById(R.id.stepsValue);

        RecyclerView recyclerView = view.findViewById(R.id.idRecyclerViewHorizontalList);
        // add a divider after each item for more clarity
//        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.HORIZONTAL));
        tileAdapter = new HomeRecyclerViewAdapter(tileList, getActivity());
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager);
        recyclerView.setAdapter(tileAdapter);
        populateTileList();
//
        ArrayList<HomeScoreListItemModel> listItemArray = new ArrayList<HomeScoreListItemModel>();
        listItemArray.add(new HomeScoreListItemModel("11 June 2019", "", true));
        listItemArray.add(new HomeScoreListItemModel("Energy Use", "+500", false));
        listItemArray.add(new HomeScoreListItemModel("Incident Reporting", "+500", false));
        listItemArray.add(new HomeScoreListItemModel("Wrong Parking", "-500", false));

        listItemArray.add(new HomeScoreListItemModel("10 June 2019", "", true));
        listItemArray.add(new HomeScoreListItemModel("Energy Use", "+500", false));
        listItemArray.add(new HomeScoreListItemModel("Incident Reporting", "+500", false));
        listItemArray.add(new HomeScoreListItemModel("Wrong Parking", "-500", false));

        listItemArray.add(new HomeScoreListItemModel("9 June 2019", "", true));
        listItemArray.add(new HomeScoreListItemModel("Energy Use", "+500", false));
        listItemArray.add(new HomeScoreListItemModel("Incident Reporting", "+500", false));
        listItemArray.add(new HomeScoreListItemModel("Wrong Parking", "-500", false));

        listItemArray.add(new HomeScoreListItemModel("8 June 2019", "", true));
        listItemArray.add(new HomeScoreListItemModel("Energy Use", "+500", false));
        listItemArray.add(new HomeScoreListItemModel("Incident Reporting", "+500", false));
        listItemArray.add(new HomeScoreListItemModel("Wrong Parking", "-500", false));

        listItemArray.add(new HomeScoreListItemModel("7 June 2019", "", true));
        listItemArray.add(new HomeScoreListItemModel("Energy Use", "+500", false));
        listItemArray.add(new HomeScoreListItemModel("Incident Reporting", "+500", false));
        listItemArray.add(new HomeScoreListItemModel("Wrong Parking", "-500", false));


        ListView listView = view.findViewById(R.id.score_list);
        HomeScoreListArrayAdapter m_adapter = new HomeScoreListArrayAdapter(getActivity(), R.layout.home_score_list_item, R.layout.home_score_list_section_item,listItemArray );
        listView.setAdapter(m_adapter);

        return view;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sensorManager =  (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accel, SensorManager.SENSOR_DELAY_FASTEST);
        simpleStepDetector = new StepDetector();
        simpleStepDetector.registerListener(this);
        Sensor stepSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if(stepSensor != null)
        {
            sensorManager.registerListener(this, stepSensor, SensorManager.SENSOR_DELAY_UI);
        }




    }


    void populateTileList()
    {
        tileList.add(new HomeTile("Steps Walked", "5.5", FeatureType.Steps));
        tileList.add(new HomeTile("Department of Transportation", "8.9", FeatureType.Transportation));
        tileList.add(new HomeTile("Social Volunteer", "1.3", FeatureType.SocialVolunteer));
        tileList.add(new HomeTile("Energy Use", "5.5", FeatureType.EnergyUse));
        tileList.add(new HomeTile("Report Incident", "6.5", FeatureType.ReportIncident));
        tileAdapter.notifyDataSetChanged();
    }




    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
//        stepsTextView.setText("" + event.values[0]);
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            simpleStepDetector.updateAccel(
                    event.timestamp, event.values[0], event.values[1], event.values[2]);
        }
    }

    @Override
    public void step(long timeNs) {
        numSteps++;
        stepsTextView.setText("" + numSteps);
    }
}
