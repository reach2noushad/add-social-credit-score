package adda.ae.com.addademo.userinterface;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import adda.ae.com.addademo.Others.FeatureType;
import adda.ae.com.addademo.Others.HomeScoreListArrayAdapter;
import adda.ae.com.addademo.Others.ScoreScoreListArrayAdapter;
import adda.ae.com.addademo.R;
import adda.ae.com.addademo.models.HomeScoreListItemModel;
import adda.ae.com.addademo.models.ScoreScoreListItemModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ScoreFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ScoreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScoreFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ScoreFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ScoreFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ScoreFragment newInstance(String param1, String param2) {
        ScoreFragment fragment = new ScoreFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_score, container, false);

        populateList(view);
        return view;
    }


    void populateList(View view)
    {
        ArrayList<ScoreScoreListItemModel> listItemArray = new ArrayList<ScoreScoreListItemModel>();
        listItemArray.add(new ScoreScoreListItemModel("11 June 2019", FeatureType.Steps, true));

        ///adding sub sections for the above date
        for(int i =0;i<10;i++)
        {

            ScoreScoreListItemModel model = null;
            int d = i%5;
            switch (d)
            {
                case 0:
                    model = new ScoreScoreListItemModel("Walk", FeatureType.Steps, false);
                    model.firstDescription = "Steps Walked";
                    model.firstValue = "10,000";
                    model.score = "+100";
                    break;
                case 1:
                    model = new ScoreScoreListItemModel("Department of Transport", FeatureType.Transportation, false);
                    model.firstDescription = "Zero Parking Tickets";
                    model.firstValue = "";
                    model.score = "+50";
                    model.secondDescription = "Location";
                    model.secondValue = "Ajman";
                    model.thirdDescription = "Time";
                    model.thirdValue = "2.00PM";

                    break;
                case 2:
                    model = new ScoreScoreListItemModel("Social Volunteer", FeatureType.SocialVolunteer, false);
                    model.firstDescription = "Blood Donation";
                    model.firstValue = "";
                    model.score = "+110";
                    break;
                case 3:
                    model = new ScoreScoreListItemModel("Energy Use", FeatureType.EnergyUse, false);
                    model.firstDescription = "Saved Electricity";
                    model.firstValue = "20 units";
                    model.score = "+80";
                    break;
                default:
                    model = new ScoreScoreListItemModel("Incident Report", FeatureType.ReportIncident, false);
                    model.firstDescription = "Triggered Fire Alarm";
                    model.firstValue = "";
                    model.score = "+130";
                    break;
            }

            listItemArray.add(model);
        }

        listItemArray.add(new ScoreScoreListItemModel("10 June 2019", FeatureType.Steps, true));

        ///adding sub sections for the above date
        for(int i =0;i<10;i++)
        {

            ScoreScoreListItemModel model = null;
            int d = i%5;
            switch (d)
            {
                case 0:
                    model = new ScoreScoreListItemModel("Walk", FeatureType.Steps, false);
                    model.firstDescription = "Steps Walked";
                    model.firstValue = "10,000";
                    model.score = "+100";
                    break;
                case 1:
                    model = new ScoreScoreListItemModel("Department of Transport", FeatureType.Transportation, false);
                    model.firstDescription = "Zero Parking Tickets";
                    model.firstValue = "";
                    model.score = "+50";
                    model.secondDescription = "Location";
                    model.secondValue = "Ajman";
                    model.thirdDescription = "Time";
                    model.thirdValue = "2.00PM";
                    break;
                case 2:
                    model = new ScoreScoreListItemModel("Social Volunteer", FeatureType.SocialVolunteer, false);
                    model.firstDescription = "Blood Donation";
                    model.firstValue = "";
                    model.score = "+110";
                    break;
                case 3:
                    model = new ScoreScoreListItemModel("Energy Use", FeatureType.EnergyUse, false);
                    model.firstDescription = "Saved Electricity";
                    model.firstValue = "20 units";
                    model.score = "+80";
                    break;
                default:
                    model = new ScoreScoreListItemModel("Incident Report", FeatureType.ReportIncident, false);
                    model.firstDescription = "Triggered Fire Alarm";
                    model.firstValue = "";
                    model.score = "+130";
                    break;
            }

            listItemArray.add(model);
        }

        ListView listView = view.findViewById(R.id.score_list);
        ScoreScoreListArrayAdapter m_adapter = new ScoreScoreListArrayAdapter(getActivity(), R.layout.score_score_list_item, R.layout.score_score_list_section_item,listItemArray );
        listView.setAdapter(m_adapter);
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
