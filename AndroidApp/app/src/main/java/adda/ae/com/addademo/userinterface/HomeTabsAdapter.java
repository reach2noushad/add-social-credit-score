package adda.ae.com.addademo.userinterface;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import adda.ae.com.addademo.userinterface.HomeFragment;
import adda.ae.com.addademo.userinterface.ProfileFragment;

public class HomeTabsAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;
    public HomeTabsAdapter(FragmentManager fm, int NoofTabs){
        super(fm);
        this.mNumOfTabs = NoofTabs;

    }
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
    @Override
    public Fragment getItem(int position){
        switch (position){
            case 0:
                HomeFragment home = new HomeFragment();
                return home;
            case 1:
                ScoreFragment scoreFragment = new ScoreFragment();
                return scoreFragment;
            case 2:
                WalletFragment walletFragment = new WalletFragment();
                return walletFragment;
            case 3:
                MarketPlaceFragment marketPlaceFragment = new MarketPlaceFragment();
                return marketPlaceFragment;
            default:
                return null;
        }
    }
}
