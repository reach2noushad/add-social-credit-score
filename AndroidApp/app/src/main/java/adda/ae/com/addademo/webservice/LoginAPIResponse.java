package adda.ae.com.addademo.webservice;

import adda.ae.com.addademo.webservice.IRetrofitResponse;

public class LoginAPIResponse implements IRetrofitResponse {

    public String message;
    public String userId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
