package adda.ae.com.addademo.webservice;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IAPIs {



    @POST("user/login")
    Call<LoginAPIResponse> authenticateUser(@Body LoginAPIRequest user);


}
