package adda.ae.com.addademo.models;

import adda.ae.com.addademo.Others.FeatureType;

public class ScoreScoreListItemModel {


    public ScoreScoreListItemModel(String description, FeatureType type, boolean isSection)
    {
        this.header = description;
        this.type = type;
        this.isSection = isSection;
    }


    public String header;
    public boolean isSection;
    public  String score;
    public  String firstDescription;
    public  String firstValue;
    public FeatureType type;

    public  String secondDescription;
    public  String secondValue;

    public  String thirdDescription;
    public  String thirdValue;
}
