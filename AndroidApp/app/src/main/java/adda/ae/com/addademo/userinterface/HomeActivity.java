package adda.ae.com.addademo.userinterface;

import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import adda.ae.com.addademo.R;

public class HomeActivity extends AppCompatActivity implements ScoreFragment.OnFragmentInteractionListener, MarketPlaceFragment.OnFragmentInteractionListener, WalletFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
//        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

//        Toolbar toolbar = (Toolbar)findViewById(R.id.tool_bar);
//        setSupportActionBar(toolbar);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Home").setIcon(R.drawable.tab_house_icon_selected));
        tabLayout.addTab(tabLayout.newTab().setText("Score").setIcon(R.drawable.tab_score_icon_unselected));
        tabLayout.addTab(tabLayout.newTab().setText("Wallet").setIcon(R.drawable.tab_wallet_icon_unselected));
        tabLayout.addTab(tabLayout.newTab().setText("Market Place").setIcon(R.drawable.tab_marketplace_icon_unselected));

        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.home_tab_selected));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager =(ViewPager) findViewById(R.id.view_pager);
        HomeTabsAdapter tabsAdapter = new HomeTabsAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(tabsAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

                tab.getIcon().setColorFilter(getResources().getColor(R.color.home_tab_selected), PorterDuff.Mode.SRC_IN);
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(getResources().getColor(R.color.home_tab_unselected), PorterDuff.Mode.SRC_IN);

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
