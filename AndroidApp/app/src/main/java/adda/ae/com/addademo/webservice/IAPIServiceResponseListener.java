package adda.ae.com.addademo.webservice;

public interface IAPIServiceResponseListener {

    public void onTaskComplete();
    public void updateUI();
}
