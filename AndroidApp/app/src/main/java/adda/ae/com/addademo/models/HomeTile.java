package adda.ae.com.addademo.models;

import adda.ae.com.addademo.Others.FeatureType;
import adda.ae.com.addademo.R;




public class HomeTile {

    public HomeTile(String header, String scoreValue, FeatureType type)
    {
        this.header = header;
        this.scoreValue = scoreValue;
        setImagesAndLabels(type);
    }

    void setImagesAndLabels(FeatureType type)
    {
        switch (type)
        {
            case Steps:
                this.scoreLabel = "Score";
                this.footerDrawableId = R.drawable.footsteps;
                break;
            case Transportation:
                this.scoreLabel = "Score";
                this.footerDrawableId = R.drawable.bus;
                break;
            case EnergyUse:
                this.scoreLabel = "Score";
                this.footerDrawableId = R.drawable.renewable_energy;
                break;
            case SocialVolunteer:
                this.scoreLabel = "Score";
                this.footerDrawableId = R.drawable.medal;
                break;
            case ReportIncident:
                this.scoreLabel = "Score";
                this.footerDrawableId = R.drawable.forest_fire;
                break;
        }
    }



    public String header;
    public  String scoreLabel;
    public  String scoreValue;
    public int footerDrawableId;

}
