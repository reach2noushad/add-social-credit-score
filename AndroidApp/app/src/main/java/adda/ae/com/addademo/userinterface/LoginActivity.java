package adda.ae.com.addademo.userinterface;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import adda.ae.com.addademo.R;
import adda.ae.com.addademo.userinterface.HomeActivity;
import adda.ae.com.addademo.webservice.IAPIServiceResponseListener;
import adda.ae.com.addademo.webservice.IAPIs;
import adda.ae.com.addademo.webservice.IRetrofitResponse;
import adda.ae.com.addademo.webservice.LoginAPIRequest;
import adda.ae.com.addademo.webservice.LoginAPIResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, IAPIServiceResponseListener, Callback<LoginAPIResponse>, View.OnFocusChangeListener {

    Button login,register;
    private TextInputEditText mobNumb;
    private TextInputEditText pass;
    private TextInputLayout mob_layout;
    private TextView errorText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);

        login = findViewById(R.id.btn_login);
        register = findViewById(R.id.btn_register);
        mobNumb = findViewById(R.id.userIDTextInputEditText);
        pass = findViewById(R.id.passwordTextInputEditText);
        mob_layout =  findViewById(R.id.userIDTextInputLayout);
        errorText = findViewById(R.id.errorText);

        /*set button click listner*/
        login.setOnClickListener(this);
        register.setOnClickListener(this);
        mobNumb.setOnFocusChangeListener(this);
        pass.setOnFocusChangeListener(this);
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId() == R.id.btn_login)
        {
            loginCLicked();
        }
        else if(view.getId() == R.id.btn_register){
            Intent intent = new Intent(this, UserRegisterActivity.class);
            startActivity(intent);
        }
    }

    void loginCLicked()
    {
        if((mobNumb.getText().toString().equalsIgnoreCase("")) || (pass.getText().toString().equalsIgnoreCase(""))){
            showInvalidLoginMessage(1);
        }
        else{
            serviceCall();
        }
    }

    @Override
    public void onTaskComplete() {

    }

    @Override
    public void updateUI() {

    }

    void serviceCall()
    {
        String mobNum, mPass;
        mobNum = mobNumb.getText().toString();//00971501234567
        mPass = pass.getText().toString();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://115.114.36.146:2122/api/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        IAPIs gerritAPI = retrofit.create(IAPIs.class);
        LoginAPIRequest loginAPIRequest = new LoginAPIRequest();
        loginAPIRequest.mobileNo = mobNum;
        loginAPIRequest.password = mPass;

        Call<LoginAPIResponse> call = gerritAPI.authenticateUser(loginAPIRequest);
        call.enqueue(this);
    }
    @Override
    public void onResponse(Call<LoginAPIResponse> call, Response<LoginAPIResponse> response) {
        if(response.isSuccessful()){
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        }else{
            showInvalidLoginMessage(2);
        }
    }

    private void showInvalidLoginMessage(int i) {
        switch (i){
            case 1:
                errorText.setVisibility(View.VISIBLE);
                errorText.setText("Fields should not be blank!");
                break;
            case 2:
                errorText.setVisibility(View.VISIBLE);
                errorText.setText("Invalid Credential");
                break;
            default:
        }

    }

    @Override
    public void onFailure(Call<LoginAPIResponse> call, Throwable t) {

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        errorText.setVisibility(View.GONE);
    }
}
