package adda.ae.com.addademo.Others;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import adda.ae.com.addademo.R;
import adda.ae.com.addademo.models.HomeScoreListItemModel;

public class HomeScoreListArrayAdapter extends ArrayAdapter {


    Context context;
    ArrayList<HomeScoreListItemModel> itemModel;
    int resourceId;
    int sectionResourceId;

    public HomeScoreListArrayAdapter(Context applicationContext, int resource, int sectionResourceId, ArrayList<HomeScoreListItemModel> text){
        super(applicationContext, resource);
        this.context=applicationContext;
        this.itemModel=text;
        this.resourceId = resource;
        this.sectionResourceId = sectionResourceId;
    }
    @Override
    public int getCount() {
        return itemModel.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View listrow= null;
        HomeScoreListItemModel model = itemModel.get(position);
        if(model.isSection)
        {
            listrow = inflater.inflate(this.sectionResourceId,null);//R.layout.home_score_list_item
            TextView textView = (TextView) listrow.findViewById(R.id.description);
            textView.setText(model.description);
        }else{
            listrow = inflater.inflate(this.resourceId,null);//R.layout.home_score_list_item
            TextView textView = (TextView) listrow.findViewById(R.id.description);
            textView.setText(model.description);
            TextView textViewValue = (TextView) listrow.findViewById(R.id.value);
            textViewValue.setText(model.value);
        }

        return listrow;
    }
}
