package adda.ae.com.addademo.webservice;

public interface RequestListener<T> {

    void onSuccess(T response);

    void onResponse();

    void onError();
}
