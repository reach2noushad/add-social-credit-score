package adda.ae.com.addademo.Others;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import adda.ae.com.addademo.R;
import adda.ae.com.addademo.models.HomeTile;

public class HomeRecyclerViewAdapter extends RecyclerView.Adapter {

    private List<HomeTile> horizontalHomeTileList;
    Context context;

    public HomeRecyclerViewAdapter(List<HomeTile> horizontalGrocderyList, Context context){
        this.horizontalHomeTileList= horizontalGrocderyList;
        this.context = context;
    }
    @Override
    public HomeTileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View homeTileView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_recycler_menu_item, parent, false);
        HomeTileViewHolder gvh = new HomeTileViewHolder(homeTileView);
//        LayoutParams layoutParams = (LinearLayout.LayoutParams) homeTileView.getLayoutParams();
//        layoutParams.width = parent.getMeasuredWidth() / 4;
//        homeTileView.getLayoutParams().width = parent.getMeasuredWidth() / 4;

        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //if you need three fix imageview in width
        int devicewidth = displaymetrics.widthPixels / 3;

        //if you need 4-5-6 anything fix imageview in height
        int deviceheight = displaymetrics.heightPixels / 4;


        homeTileView.getLayoutParams().width = devicewidth;
        homeTileView.getLayoutParams().height = deviceheight;

        return gvh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if(viewHolder instanceof  HomeTileViewHolder)
        {
            ((HomeTileViewHolder)viewHolder).txtview.setText(horizontalHomeTileList.get(i).header);
            ((HomeTileViewHolder)viewHolder).scoreTextViewLabel.setText(horizontalHomeTileList.get(i).scoreLabel);
            ((HomeTileViewHolder)viewHolder).scoreTextView.setText(horizontalHomeTileList.get(i).scoreValue);
            ((HomeTileViewHolder)viewHolder).footerImageView.setImageResource(horizontalHomeTileList.get(i).footerDrawableId);
        }

    }

//    @Override
//    public void onBindViewHolder(HomeTileViewHolder holder, final int position) {
////        holder.imageView.setImageResource(horizontalGrocderyList.get(position).getProductImage());
//        holder.txtview.setText(horizontalHomeTileList.get(position).header);
////        holder.imageView.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                String productName = horizontalGrocderyList.get(position).getProductName().toString();
////                Toast.makeText(context, productName + " is selected", Toast.LENGTH_SHORT).show();
////            }
////        });
//    }

    @Override
    public int getItemCount() {
        return horizontalHomeTileList.size();
    }

    public class HomeTileViewHolder extends RecyclerView.ViewHolder {
//        ImageView imageView;
        TextView txtview;
        TextView scoreTextViewLabel;
        TextView scoreTextView;
        ImageView footerImageView;
        public HomeTileViewHolder(View view) {
            super(view);
//            imageView=view.findViewById(R.id.idProductImage);
            txtview=view.findViewById(R.id.home_tile_header);
            scoreTextViewLabel = view.findViewById(R.id.home_tile_score_label);
            scoreTextView = view.findViewById(R.id.home_tile_score_value);
            footerImageView = view.findViewById(R.id.home_tile_footer_image);
        }
    }
}
