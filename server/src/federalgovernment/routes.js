import express from "express";
import controller from "./controller";
const router = express.Router();



router.get('/', async (req, res, next) => {

    console.log("calling federal governemnt api");

    try {

        const {supply,transactions,userPosession,merchantPosession,reserve} = await controller.getAllDetails();
          
        res.status(200).send({supply,transactions,userPosession,merchantPosession,reserve});
           
    }
    catch (e) {
        res.status(500).send(e);
    }
});


module.exports = router;