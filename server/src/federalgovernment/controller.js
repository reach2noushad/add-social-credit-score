import config from "../config";
import walletController from "../wallet/helper"

let apiKey = "36DSQW61DJS3HMM5IN9FZUZRSKHQEC5VUT";

const api = require('etherscan-api').init(apiKey,'rinkeby', '3000');

// var balance = api.account.balance(federalGovtAccount);
// balance.then(function(balanceData){
//   console.log(balanceData);
// });


// api.log.getLogs("0xaE6CeC7DF29De4A52507B5c7168062b18C4e34cf",'4563619',"0x9ed053bb818ff08b8353cd46f78db1f0799f31c9e4458fdb425c10eccd2efc44")
// .then(function(result) {
//  console.log(result);
// });

// var supply = api.stats.tokensupply(null, '0xaE6CeC7DF29De4A52507B5c7168062b18C4e34cf');
// supply.then(function(balanceData){
//   console.log(balanceData);
// });

// var supply = api.account.tokenbalance(
//     '0x9f13ab67b7ef43a74471f02d9cb3d57854669002',
//     '',
//     '0xaE6CeC7DF29De4A52507B5c7168062b18C4e34cf' // DGD contract address
// );

// supply.then(function(balanceData){
//     console.log(balanceData);
//   });



// var txlist = api.account.txlist(config.tokenContractDeployedAddress, 1, 'latest', 1, 100, 'asc');
  
// txlist.then(function(balanceData){
//     console.log(balanceData);
//   });

//list of erc 20 txns

// var txlist = api.account.tokentx(config.addaAccountAddress, config.tokenContractDeployedAddress, 4569250, 'latest', 'asc');

// txlist.then(function(balanceData){
//     console.log(balanceData);
//   });

// api.stats.tokensupply(null, config.tokenContractDeployedAddress)
// .then(function(balanceData){
//         console.log(balanceData);
//       });

// api.account.tokenbalance("0x3c0b8cb523304079e76132544c282a3d86ebc0f7",'',config.tokenContractDeployedAddress)
// .then(function(balanceData){
//         console.log(balanceData);
//       });

function convertDate(timestamp) {

    var txnDate = new Date(timestamp);
    var date = txnDate.getDate();
    var month = txnDate.getMonth();
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let response =  date + " " +monthNames[month];
    return response;
}

module.exports = {

    getUserEvents : async(userAddress) => {

        const allEvents = await api.account.tokentx(userAddress,config.tokenContractDeployedAddress,456920,'latest','asc');

        let txns = allEvents.result;

        let result = [];

        for (const i in txns) {

            let eventParsed = txns[i];

            let event = {};

            if(eventParsed.from.toLowerCase() === userAddress.toLowerCase()) {

                event.type = "spent";
                event.to = eventParsed.to;
                event.name = config.ethereumAccountNames[0][eventParsed.to];
                event.token = (eventParsed.value)/100;  //token having two decimal places
                event.date = convertDate(eventParsed.timeStamp*1000);

                result.push(event);
  
            } else if (eventParsed.to.toLowerCase() === userAddress.toLowerCase()) {

                event.type = "earned";
                event.from = eventParsed.from;
                event.name = config.ethereumAccountNames[0][eventParsed.from];
                event.token = (eventParsed.value)/100;  //token having two decimal places
                event.date = convertDate(eventParsed.timeStamp*1000);

                result.push(event);

            }


        }

        return result;

    },

    getAllDetails : async () => {

        const totalTokens = await api.stats.tokensupply(null, config.tokenContractDeployedAddress);
        
        let supply = +(totalTokens.result)/100;

        const txlist = await walletController.getTransferEvents();

        const parsedEvents = await walletController.getAllTransaction(txlist);

        let merchantTokens = 0;
        let userTokens = 0;

        //find merchant held coins
        // subtract merchant held tokens from main token supplu to get users held tokens

        for (const i in config.merchantAddressList){

            const individualMerchantToken = await api.account.tokenbalance(config.merchantAddressList[i],'',config.tokenContractDeployedAddress)

            merchantTokens = merchantTokens + +(individualMerchantToken.result)/100; //since token  has two decimal places

        }

        const reserveToken = await api.account.tokenbalance("0x3c0b8cb523304079e76132544c282a3d86ebc0f7",'',config.tokenContractDeployedAddress);

        let reserveBalance = +(reserveToken.result)/100;

        userTokens = supply - merchantTokens - reserveBalance;

        return {supply:supply,transactions:[parsedEvents],userPosession:userTokens,merchantPosession:merchantTokens,reserve:reserveBalance}

    }

}
