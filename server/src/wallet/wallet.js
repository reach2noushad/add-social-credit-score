import transactions from '../transactions/quorumtransaction';



module.exports = {

    getTokenBalance : async (ethereumAddress) => {

        const balance = await transactions.getTokenBalance(ethereumAddress);
        return balance;

    }

}