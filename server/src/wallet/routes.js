import express from "express";
import controller from "./controller";
const router = express.Router();


router.get('/transactions', async (req, res, next) => {

    console.log("calling this api");

    try {
        const data = req.query;

        if(data.userId) {
            
            const {userStatus,userAddress} = await controller.getUserEthereumAddress(data.userId);
            console.log("this is userStatus",userStatus);        
            if(userStatus){
                const transactions = await controller.getUserTransactions(userAddress);
                res.status(200).send(transactions);
            } else {
                res.status(200).send([]);
            }

        } else {

            res.status(422).send({message : "Invalid Query Parameter"})

        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});




module.exports = router;