const ethers = require('ethers');
const utils = require('ethers').utils;
const Interface = ethers.Interface;
import config from '../config';

const abi = config.tokenContractAbi;
const iface = new Interface(abi);

const provider = ethers.providers.getDefaultProvider('rinkeby');
const eventInfo = iface.events;
const contractAddress = config.tokenContractDeployedAddress;


function convertDate(timestamp) {

    var txnDate = new Date(timestamp);
    var date = txnDate.getDate();
    var month = txnDate.getMonth();
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let response =  date + " " +monthNames[month];
    return response;
}

async function getAllEvents(eventTopic) {

    var filter = {
        fromBlock: 4563615,
        address: contractAddress,
        topics: [ eventTopic ]
     }

     var eventLogs = await provider.getLogs(filter);

    return eventLogs;

}

async function getAllTransactionEvents(eventLogs,eventName) {

    let newEventInfo = eventInfo[eventName];
    var result = [];

      for(const i in eventLogs){

        var eventParsed = newEventInfo.parse(eventLogs[i].topics, eventLogs[i].data);
  
        let individualEvent ={};

        individualEvent.from = eventParsed.from;
        individualEvent.fromName = config.ethereumAccountNames[0][eventParsed.from.toLowerCase()];
        individualEvent.to = eventParsed.to;
        individualEvent.toName = config.ethereumAccountNames[0][eventParsed.to.toLowerCase()];
        individualEvent.tokens = eventParsed.tokens.toNumber()/100;
        individualEvent.time = new Date(eventParsed.time.toNumber()*1000).toLocaleString("en-US", {timeZone: "Asia/Calcutta"});

        result.push(individualEvent);

    }

    return result;



}

async function getEventsForAddress (address,eventLogs,eventName) {

    let newEventInfo = eventInfo[eventName];
    var result = [];

    
  

    // for(const i in eventLogs){

    //     var eventParsed = newEventInfo.parse(eventLogs[i].topics, eventLogs[i].data);
    //     console.log("here",eventParsed);
    //     let individualEvent ={};

    //     if(eventParsed.from.toLowerCase() === address.toLowerCase()) {

    //         individualEvent.type = "spent";
    //         individualEvent.to = eventParsed.to;
    //         individualEvent.name = config.ethereumAccountNames[0][eventParsed.to];
    //         individualEvent.token = (eventParsed.tokens.toNumber())/100; //token having two decimal places 
    //         individualEvent.date = convertDate(eventParsed.time.toNumber()*1000);
    //         result.push(individualEvent);

    //      } else if (eventParsed.to.toLowerCase() === address.toLowerCase()) {

    //         individualEvent.type = "earned";
    //         individualEvent.from = eventParsed.from;
    //         individualEvent.name = config.ethereumAccountNames[0][eventParsed.from];
    //         individualEvent.token = (eventParsed.tokens.toNumber())/100;  //token having two decimal places
    //         individualEvent.date = convertDate(eventParsed.time.toNumber()*1000);
    //         result.push(individualEvent);
    //     }
       
    // }

    // return result;
  
}


module.exports = {

    getTransferEvents : async() => {

        let eventTopic = config.events[0].tokenTransfers;
        const events = await getAllEvents(eventTopic);
        return events;

    },

    getUserEvents : async (userAddress, allTransferEvents) => {

        const events = await getEventsForAddress(userAddress,allTransferEvents,'tokenTransfers');
        return events;
        
    },

    getAllTransaction : async(allTransferEvents) => {

        const events = await getAllTransactionEvents(allTransferEvents,'tokenTransfers');
        return events;



    }

}