
import userRepo from '../user/userRepo';
import events from '../transactions/events';

module.exports = {

    getUserEthereumAddress : async (userId) => {

        const userDetails = await userRepo.getUser(userId);
        if (userDetails){
        const ethereumAddress = userDetails[0].ethereumAddress;
        let hexifyedAccountAddress = "0x"+ethereumAddress;
        return {userStatus:true, userAddress:hexifyedAccountAddress};
        }else {
            return {userStatus:false};
        }

    },

    getUserTransactions : async (userAddress) => {
        return await events.getUserEvents(userAddress);
    }
}