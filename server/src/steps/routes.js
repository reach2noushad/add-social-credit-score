import express from "express";
import controller from "./controller";
const router = express.Router();



router.post('/', async (req, res, next) => {

    try {

        const data = req.body;

        console.log(data);

        if (data.userId && data.steps) {

            const {updatedSteps, updatedScore} = await controller.saveUserSteps(data.userId,data.steps);

            await controller.updateUserMainScore(data.userId,data.steps);

            res.status(200).send({totalSteps:updatedSteps, scoreBySteps : updatedScore});
            
        }else{

            res.status(422).send({message : "Invalid Query Parameter"})
        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});









module.exports = router;