import config from "../config";

module.exports = {

    convertStepsIntoScore : async(steps) => {
        let score = +(steps) / +(config.stepToScoreValue);
        return score;
    }
}