import stepsRepo from "./stepsRepo";
import helper from "./helper";

module.exports = {

    saveUserSteps : async (userId,steps) => {

        let todaysDate = new Date();
        let today = todaysDate.toISOString().slice(0, 19).split('T')[0];
   
        const userDetailsofStepsToday = await stepsRepo.IsUserHavingSteps(userId,today);
   
        if(userDetailsofStepsToday.status) {

            let existingStepsOfUser = userDetailsofStepsToday.steps;
            let existingStepScoreOfUser = userDetailsofStepsToday.score;

            let newStepsOfUser = +(existingStepsOfUser) + +(steps);

            let scoreForNewSteps = await helper.convertStepsIntoScore(steps);
            let newStepScoreOfUser = +(existingStepScoreOfUser) + +(scoreForNewSteps);

            await stepsRepo.updateUserSteps(userId,today,newStepsOfUser,newStepScoreOfUser);

            return {updatedSteps : newStepsOfUser, updatedScore : newStepScoreOfUser.toFixed(2)};

        } else {
            

            //new user steps for the day or user first time logging steps
            let scoreForSteps = await helper.convertStepsIntoScore(steps);

            await stepsRepo.insertNewUserSteps(userId,today,steps,scoreForSteps);

            return {updatedSteps : steps, updatedScore : scoreForSteps.toFixed(2)};


        }
 
    },

    updateUserMainScore : async (userId,steps) => {

        const presentUserTotalScore = await stepsRepo.getUserTotalScore(userId);

        let scoreForSteps = await helper.convertStepsIntoScore(steps);

        let newScore = +(presentUserTotalScore) + +(scoreForSteps);

        await stepsRepo.updateUserTotalScore(userId,newScore);

    }
}


