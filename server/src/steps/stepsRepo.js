import DBService from "../services/dbservice";

module.exports = {

    IsUserHavingSteps : async(userId,date) => {

        let stepsDateQuery = 'SELECT * FROM userscoretransactions WHERE userId = ? AND date = ? AND transactiontype =?';
        let stepsDateQueryResult = await DBService.executeQuery({query: stepsDateQuery, values: [userId,date,'steps']});

        if (stepsDateQueryResult.length > 0) {
            return {status:true,steps:stepsDateQueryResult[0].stepcount,score:stepsDateQueryResult[0].score};
        } else {
            return {status:false};
        }
    },

    updateUserSteps : async (userId,date,steps,score) => {

        return new Promise(async (resolve, reject) => {
        try {
            const query_params = {
                table: "userscoretransactions",
                set: {
                    stepcount: steps,
                    score: score
                },
                where: {
                    userId: userId,
                    date:date,
                    transactiontype:"steps"
                }
            };

            await DBService.updateQuery(query_params,"AND");
            resolve();
        } catch (err) {
            reject(err);
        }
    });


    },

    insertNewUserSteps : async (userId,date,steps,score) => {

        return new Promise(async (resolve, reject) => {
            try {
                const values = [userId,'steps', date, steps,score];
                const query_params = {
                    table: "userscoretransactions",
                    columns: ["userId", "transactiontype", "date","stepcount","score"],
                    values: [values]
                };
                await DBService.insertQuery(query_params);
                resolve();
            } catch (err) {
                reject(err);
            }
        });


    },

    getUserTotalScore : async (userId) => {

        let userScoreQuery = 'SELECT * FROM userscore WHERE userId = ?';
        let userScoreQueryResult = await DBService.executeQuery({query: userScoreQuery, values: [userId]});
  
        if (userScoreQueryResult.length > 0) {
            return userScoreQueryResult[0].totalscore;
        }

    },

    updateUserTotalScore :  async (userId, score) => {


        return new Promise(async (resolve, reject) => {
            try {
                const query_params = {
                    table: "userscore",
                    set: {
                        totalscore: score
                    },
                    where: {
                        userId: userId
                    }
                };
    
                await DBService.updateQuery(query_params);
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }


}


