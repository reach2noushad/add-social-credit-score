import express from "express";
import controller from "./controller";
const router = express.Router();


router.get('/eligibleToRedeem', async (req, res, next) => {

    try {

        const data = req.query;

        if (data.userId) {

            const isUserEligible = await controller.checkUserEligibilty(data.userId);

            if (isUserEligible.status) {
                res.status(200).send({message: "eligible"})
            } else{
                res.status(200).send({message: "not eligible"})
            }


        }else{

            res.status(422).send({message : "Invalid Query Parameter"})
        }

    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.post('/redeemCoins', async (req, res, next) => {

    try {

        const data = req.body;

        if (data.userId) {

            const userDetails = await controller.checkUserEligibilty(data.userId);

            if (userDetails.status) {

                const redeemedDetails = await controller.redeemCoinsForScore(data.userId, userDetails.redeemableScore);

                if (redeemedDetails.status) {
                    res.status(200).send({consumedScore : redeemedDetails.consumedScore,coinsObtained : redeemedDetails.redeemedCoins})
                } else{
                    res.status(200).send({message : "error redeeming tokens"})
                }


            } else{
                res.status(200).send({message: "not eligible"})
            }


        }else{

            res.status(422).send({message : "Invalid Query Parameter"})
        }

    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.post('/sendCoins', async (req, res, next) => {

    try {

        const data = req.body;

        if (data.userId && data.password && data.coins && data.toAddress) {

            const {status,userAddress} = await controller.checkUserEligibiltyForCoins(data.userId,data.coins);

            if (status) {

                console.log("user has enough tokens");

                await controller.sendCoins(userAddress,data.password,data.toAddress,data.coins);

                res.status(200).send({message:"Coins Successfully Sent"});


            } else {
                //user does not have tokens
                res.status(200).send({message : "User does not have enough coins"});
            }

            // if (userDetails.status) {

            //     const redeemedDetails = await controller.redeemCoinsForScore(data.userId, userDetails.redeemableScore);

            //     if (redeemedDetails.status) {
            //         res.status(200).send({consumedScore : redeemedDetails.consumedScore,coinsObtained : redeemedDetails.redeemedCoins})
            //     } else{
            //         res.status(200).send({message : "error redeeming tokens"})
            //     }


            // } else{
            //     res.status(200).send({message: "not eligible"})
            // }


        }else{

            res.status(422).send({message : "Invalid Query Parameter"})
        }

    }
    catch (e) {
        res.status(500).send(e);
    }
});


module.exports = router;