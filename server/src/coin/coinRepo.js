import DBService from "../services/dbservice";

module.exports = {

    getUserScores : async(userId) => {

        let userScoreQuery = 'SELECT * FROM userscore WHERE userId = ?';
        let userScoreQueryResult = await DBService.executeQuery({query: userScoreQuery, values: [userId]});
  
        console.log(userScoreQueryResult);
        if (userScoreQueryResult.length > 0) {
            return {userTotalScore : userScoreQueryResult[0].totalscore, userUsedScore : userScoreQueryResult[0].usedscore};
        }

    },

    getUserDetails : async(userId) => {

        let userQuery = 'SELECT * FROM user WHERE userId = ?';
        let userQueryResult = await DBService.executeQuery({query: userQuery, values: [userId]});

        if(userQueryResult.length > 0) {
            return userQueryResult[0].ethereumAddress;
        }
    },

    updateUsedScore : async(userId, usedscore) => {

        return new Promise(async (resolve, reject) => {
            try {
                const query_params = {
                    table: "userscore",
                    set: {
                        usedscore: usedscore
                    },
                    where: {
                        userId: userId
                    }
                };
    
                await DBService.updateQuery(query_params);
                resolve();
            } catch (err) {
                reject(err);
            }
        });


    }

}