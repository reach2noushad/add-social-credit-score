import coinRepo from "./coinRepo";
import config from "../config";
import helper from "./helper";
import userRepo from "../user/userRepo";
import userHelper from "../user/helper";

import transaction from "../transactions/quorumtransaction";
import keythereum from '../keythereum';

module.exports ={

    checkUserEligibilty : async(userId) => {

        const {userTotalScore, userUsedScore} = await coinRepo.getUserScores(userId);

        console.log(userTotalScore,userUsedScore);

        let redeemableScore = +(userTotalScore) - +(userUsedScore);

        if (redeemableScore >= config.minimumScoreRequiredToRedeemCoin) {
            //eligible
            return {status :true, totalscore :userTotalScore, usedScore : userUsedScore, redeemableScore : redeemableScore};
        } else {
            //not eligible
            return {status :false, totalscore :userTotalScore, usedScore : userUsedScore, redeemableScore : redeemableScore};
        }

    },

    redeemCoinsForScore : async (userId, redeemableScore) => {

       //find the number of times of the score eligible which the user has
       //say if user has 3000 score, user is eligible for 3 times crediting 100 coins;
       //even if it is 3999, user will get 300 coins conly

       let numberOfTimeUserGetsCoin = +(redeemableScore) / +(config.scoreToCoinRatio.score);

       let wholeNumberMultiple = Math.trunc(numberOfTimeUserGetsCoin);

       console.log(wholeNumberMultiple);

       let numberOfCoinsToCreditUser = wholeNumberMultiple * +(config.scoreToCoinRatio.coin);

       console.log(numberOfCoinsToCreditUser);

       const tokenCreditStatus = await helper.creditCoins(userId,numberOfCoinsToCreditUser);

       if (tokenCreditStatus) {

           let usedScore = wholeNumberMultiple * +(config.scoreToCoinRatio.score);

           await helper.updateUserUsedScore(userId,usedScore);

           return {status : true, redeemedCoins:numberOfCoinsToCreditUser, consumedScore : usedScore}

       } else { 

           return {status : false};
       }

    },

    checkUserEligibiltyForCoins : async (userId, coins) => {

        const userDetails = await userRepo.getUser(userId);
        const ethereumAddress = userDetails[0].ethereumAddress;
        let hexifyedAccountAddress = "0x"+ethereumAddress;
        const userTokenBalance = await userHelper.getTokenBalance(hexifyedAccountAddress);

        if (userTokenBalance => coins) {
            return {status:true,userAddress:hexifyedAccountAddress};
        } else {
            return {status:false};
        }

    },

    sendCoins : async (userAddress,password,toAddress,coins) => {

        let decimalCompensatedCoins = +(coins) * 100; //2 decimal places

        const fromAddressPrivateKey = await keythereum.getPrivateKey(userAddress,password);

        const transactionResult = await transaction.sendTokens(userAddress,fromAddressPrivateKey,decimalCompensatedCoins,toAddress);

        if(transactionResult.status == 200) {
            return true;
        } else {
            return false;
        }


    }

}