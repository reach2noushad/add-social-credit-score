import coinRepo from "./coinRepo";
import transaction from "../transactions/quorumtransaction";
import notificationsController from "../notifications/controller"

module.exports = {

    creditCoins : async (userId,numberOfCoins) => {

        //get ethereum address corresponding to user Id

        const ethereumAddress = await coinRepo.getUserDetails(userId);

        let hexifyedEthereumAddress = "0x" + ethereumAddress;

        console.log (hexifyedEthereumAddress, numberOfCoins);

        let realNumberOfCoins = numberOfCoins * 100; //coin having two decimal places

        const creditTransaction = await transaction.creditTokens(hexifyedEthereumAddress,realNumberOfCoins);

        console.log(creditTransaction.status);

        if (creditTransaction.status == 200) {
            //notify user if eligible for social welfare fund status
            notificationsController.checkAndNotifyUser(userId,ethereumAddress);
            return true;
        } else {
            return false;
        }
    },

    updateUserUsedScore : async(userId,redeemedScore) => {

        const userScoreDetails = await coinRepo.getUserScores(userId);
        let userUsedScore = userScoreDetails.userUsedScore;

        //append the redeemedscore
        let updatedUsedScore = +(userUsedScore) + +(redeemedScore);

        await coinRepo.updateUsedScore(userId,updatedUsedScore);
    }
}