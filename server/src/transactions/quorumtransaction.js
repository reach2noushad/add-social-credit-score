

const Web3 = require('web3');

import config from '../config';
import helper from './helper';
import keythereum from '../keythereum';

const web3 = new Web3(new Web3.providers.HttpProvider(config.providerURL + ":" + config.providerPort));

const contract = new web3.eth.Contract(config.tokenContractAbi,config.tokenContractDeployedAddress);

module.exports = {

    creditTokens : async (userAddress,coins) => {

        try {

            let addaAccountDetails = await keythereum.getAddaAccountAndPrivateKey();
            
            console.log(addaAccountDetails);

            let addaAccountAddress =  addaAccountDetails.address;

            const nonce = await helper.getNonceOfAccount(addaAccountAddress);

            let dataToEncode = await contract.methods.transfer(userAddress,coins);
            const data = dataToEncode.encodeABI()

            const txObj = await helper.createTxObject(nonce,data);

            let addaAccountPrivateKey = addaAccountDetails.privateKey;

            const tx = await helper.signTx(txObj,addaAccountPrivateKey);

            const receipt = await web3.eth.sendSignedTransaction(tx);

            if(receipt.status) {

                return {
                    status: 200,
                    message: "Transaction mined. Execution success"
                };
            }

        } catch (error) {

            console.log(error);
        }

    },

    sendTokens : async (fromUserAddress,fromAddressPrivateKey,coins,toAddress) => {

        try {

            console.log("see this",fromUserAddress,fromAddressPrivateKey,coins,toAddress);

            const nonce = await helper.getNonceOfAccount(fromUserAddress);

            if(coins == 15000) {
                coins = 1500;
            }


            let dataToEncode = await contract.methods.transfer(toAddress,coins);
            console.log("data to encode",dataToEncode);
            const data = dataToEncode.encodeABI();
    
            const txObj = await helper.createTxObject(nonce,data);
    
            let privateKeyWithout0x = fromAddressPrivateKey.replace("0x", "");
    
            let privateKey = Buffer.from(privateKeyWithout0x, "hex");
    
            const tx = await helper.signTx(txObj,privateKey);

            

            try {

                const receipt = await web3.eth.sendSignedTransaction(tx);

                if(receipt.status) {

                    return {
                        status: 200,
                        message: "Transaction mined. Execution success"
                    };
                }
     
                
            } catch (error) {
                console.log(error);
                
            }

            
        } catch (error) {

            console.log(error);

        }
    },

    isUserPresentInSocialWelfareStatus : async (userAddress) => {

        try {

            const status = await contract.methods.socialWelfareFundStatus(userAddress).call()

            console.log(status);

            return status;
            
        } catch (error) {

            console.log(error);
            
        }

    },

    getTokenBalance : async (userAddress) => {

        try {

            const balance = await contract.methods.balanceOf(userAddress).call()

            return balance;
            
        } catch (error) {
            
        }


    }

}

// config.addaAccountAddress();