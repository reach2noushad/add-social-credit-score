const Web3 = require('web3');

import config from '../config';


const web3 = new Web3(new Web3.providers.HttpProvider(config.providerURL + ":" + config.providerPort));

const contract = new web3.eth.Contract(config.tokenContractAbi,config.tokenContractDeployedAddress);

function convertDate(timestamp) {

    var txnDate = new Date(+(timestamp));
    var date = txnDate.getDate();
    var month = txnDate.getMonth();
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let response =  date + " " +monthNames[month];
    return response;
}


module.exports = {


    getUserEvents : async function(userAddress) {

        const txns = await contract.getPastEvents('tokenTransfers',{fromBlock:0,toBlock:'latest'});

        let result = [];

        for (const i in txns) {

            let unparsedEvents = txns[i];

 
            let eventParsed = unparsedEvents.returnValues;

       
            let event = {};

            if(eventParsed.from.toLowerCase() === userAddress.toLowerCase()) {

                event.type = "spent";
                event.to = eventParsed.to;
                event.name = config.ethereumAccountNames[0][eventParsed.to];
                event.token = (eventParsed.tokens)/100;  //token having two decimal places
                event.date = convertDate(eventParsed.time.substring(0, 10));
                console.log(event);
                result.push(event);
  
            } else if (eventParsed.to.toLowerCase() === userAddress.toLowerCase()) {
                
                console.log("inside earned");

                console.log(eventParsed);

                event.type = "earned";
                event.from = eventParsed.from;
                event.name = config.ethereumAccountNames[0][eventParsed.from];
                event.token = (eventParsed.tokens)/100;  //token having two decimal places
                event.date = convertDate(eventParsed.time.substring(0, 10));
     
                result.push(event);

            }


        }


        return result;

    }




}