const ethers = require('ethers');
import config from '../config';


function instantiateWallet(privateKey) {
    var wallet = new ethers.Wallet(privateKey);
    wallet.provider = ethers.providers.getDefaultProvider('rinkeby');
    return wallet;
}

var provider = ethers.providers.getDefaultProvider('rinkeby');

module.exports = {

    provider: ethers.providers.getDefaultProvider('rinkeby'),

    // transferEthers: async (userAddress) => {
    //     console.log("here",userAddress);
    //     try {
    //         const amount = ethers.utils.parseEther('0.5');
    //         const wallet = new ethers.Wallet(config.ethersLoadAccountPrivateKey,provider);
    //         const tx = await wallet.send(userAddress, amount);
    //         if(tx && tx.hash) {
    //             await provider.waitForTransaction(tx.hash);
    //             const status = await provider.getTransactionReceipt(tx.hash);
    //             if (status.status === 1) {
    //                 return {
    //                     status: 200,
    //                     message: "Transaction mined. Execution success"
    //                 };
    //             }
    //             else if (status.status === 0){
    //                 throw {
    //                     status: 500,
    //                     message: "Transaction mined but execution failed."
    //                 }
    //             }
    //         }
    //         else {
    //             throw {
    //                 status: 500,
    //                 message: "Transaction failed.",
    //                 error: tx
    //             }
    //         }
    //     }
    //     catch (e) {
    //         throw {
    //             status : 500,
    //             error:e
    //         }
    //     }
    // },

    creditTokens : async (userAddress,coins) => {

        try {

            const addaAccountPrivateKey = await config.addaAccountPrivateKey();

            const wallet = new ethers.Wallet(addaAccountPrivateKey,provider);
            const contract = new ethers.Contract(config.tokenContractDeployedAddress, config.tokenContractAbi, wallet);

            var overrideOptions = {
                gasLimit: 250000,
                gasPrice: 9000000000,
                nonce: 0,
                value: ethers.utils.parseEther('0')
            };
            
            console.log(overrideOptions);
            
            const tx = await contract.transfer(userAddress,coins,overrideOptions);

            console.log("txn",tx);

            if(tx && tx.hash) {
                console.log("waiting for transaction");
                await provider.waitForTransaction(tx.hash);
                const status = await provider.getTransactionReceipt(tx.hash);
                if (status.status === 1) {
                    return {
                        status: 200,
                        message: "Transaction mined. Execution success"
                    };
                }
                else if (status.status === 0){
                    throw {
                        status: 500,
                        message: "Transaction mined but execution failed."
                    }
                }
            }
            else {
                throw {
                    status: 500,
                    message: "Transaction failed.",
                    error: tx
                }
            }

        } catch(e) {

            throw e;
        }
    },

    sendTokens : async (fromAddressPrivateKey,coins,toAddress) => {

        try {

            const wallet = new ethers.Wallet(fromAddressPrivateKey,provider);
            const contract = new ethers.Contract(config.tokenContractDeployedAddress, config.tokenContractAbi, wallet);

            var overrideOptions = {
                gasLimit: 250000,
                gasPrice: 9000000000,
                nonce: 0,
                value: ethers.utils.parseEther('0')
            };
            
            const tx = await contract.transfer(toAddress,coins, overrideOptions);

            console.log("txn",tx);

            if(tx && tx.hash) {
                console.log("waiting for transaction");
                await provider.waitForTransaction(tx.hash);
                const status = await provider.getTransactionReceipt(tx.hash);
                if (status.status === 1) {
                    return {
                        status: 200,
                        message: "Transaction mined. Execution success"
                    };
                }
                else if (status.status === 0){
                    throw {
                        status: 500,
                        message: "Transaction mined but execution failed."
                    }
                }
            }
            else {
                throw {
                    status: 500,
                    message: "Transaction failed.",
                    error: tx
                }
            }

        } catch(e) {

            throw e;
        }
    },

    isUserPresentInSocialWelfareStatus : async (userAddress) => {

        const contract = new ethers.Contract(config.tokenContractDeployedAddress, config.tokenContractAbi, provider);
        
        console.log("this is user address",userAddress);

        const status = await contract.socialWelfareFundStatus(userAddress);

        console.log("this is user social welfare status",status);

        return status;

    }

}