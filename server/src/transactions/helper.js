const Web3 = require('web3');
const EthereumTx = require('ethereumjs-tx');
import config from '../config';

const web3 = new Web3(new Web3.providers.HttpProvider(config.providerURL + ":" + config.providerPort));


module.exports = {

    getNonceOfAccount : async function(accountAddress) {
        return await web3.eth.getTransactionCount(accountAddress);
    },

    createTxObject : async function(nonce,data) {

        let txParams = {
            nonce: web3.utils.toHex(nonce),
            gasPrice: '0x00',
            gasLimit: '0x47b760',
            to: config.tokenContractDeployedAddress, 
            value: '0x00',
            data,
            chainId: 10,
          };

          return txParams;
    },

    signTx : async function (txParams,privateKey) {

        const tx = new EthereumTx(txParams);
        await tx.sign(privateKey);
        const serializedTx = tx.serialize();
        var rawTx = '0x' + serializedTx.toString('hex');
        console.log('raw transaction: ', rawTx);

        return rawTx;
    }

}