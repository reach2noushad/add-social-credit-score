import express from "express";
import controller from "./controller";
const router = express.Router();

router.post('/addproduct', async (req, res, next) => {

    try {

        const data = req.body;
           

        if (data.productName && data.priceInCoins && data.merchantName && data.merchantEthAddress) {

            await controller.saveProduct(data.productName, data.priceInCoins, data.merchantName, data.merchantEthAddress)

            res.status(200).send({message: "success"});

        }else{

            res.status(422).send({message : "Invalid Input Parameter"})
        }

        

    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/listproducts', async (req, res, next) => {

    try {

    const products = await controller.getAllProducts();

    res.status(200).send({products});

    }
    catch (e) {
        res.status(500).send(e);
    }
});


router.post('/buy', async (req, res, next) => {

    try {

        console.log("hitting buy");

        const data = req.body;
           

        if (data.userId && data.password && data.productName && data.priceInCoins && data.merchantName && data.merchantEthAddress) {

            const userDetails = await controller.checkUserHasCoins(data.userId, data.priceInCoins);
         
            console.log(userDetails);


            if (userDetails.status){
                //user has enough coins buy it
                console.log("user has enough coins");

                const result = await controller.sendCoinsToMerchant(userDetails.userAddress,data.priceInCoins,data.merchantEthAddress,data.password);

                if (result) {
                    res.status(200).send({message:"The product will be shipped to your address"});
                }


            } else {

                res.status(200).send({message: "You do not have enough coins to buy the product"});

            }

        }else{

            res.status(422).send({message : "Invalid Input Parameter"})
        }
       

    }
    catch (e) {
        res.status(500).send(e);
    }
});


module.exports = router;