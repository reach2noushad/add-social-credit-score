import DBService from "../services/dbservice";

module.exports = {

    insertProduct : async(productName, priceInCoins, merchantName, merchantEthAddress) => {

        try {

            return new Promise(async (resolve, reject) => {
                try {
                    const values = [productName, priceInCoins, merchantName, merchantEthAddress ];
                    const query_params = {
                        table: "marketplace",
                        columns: ["productname", "priceincoins", "merchantname", "merchantethereumaddress"],
                        values: [values]
                    };
                    await DBService.insertQuery(query_params);
                    resolve();
                } catch (err) {
                    reject(err);
                }
            });

        }catch(e){
            throw e;
        }
    },

    listAllProducts : async() => {

        try{

            let productQuery = 'SELECT * FROM marketplace';
            let productQueryResult = await DBService.executeQuery({query: productQuery});
            if(productQueryResult.length > 0) {
                return productQueryResult;
            } else {
                return false;
            }
            
        }catch(err){
            throw err;
        }

    }
}