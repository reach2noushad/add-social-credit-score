import transaction from "../transactions/quorumtransaction";
import keythereum from '../keythereum';
import config from '../config';


module.exports = {

    sendCoins : async (fromAddress, coins, toAddress,userPassword) => {

        console.log("called sendcoins api");

        console.log(fromAddress, coins, toAddress,userPassword);
   
        const fromAddressPrivateKey = await keythereum.getPrivateKey(fromAddress,userPassword);

        console.log("got user private key",fromAddressPrivateKey);
      
        let coinsToUser = +(coins) * 100 ; //since coin have two decimal places 

        const transactionResult = await transaction.sendTokens(fromAddress,fromAddressPrivateKey,coinsToUser,toAddress);

        if(transactionResult.status == 200) {
            return true;
        } else {
            return false;
        }
    }
}