import marketplaceRepo from "./marketplaceRepo";
import userWallet from "../user/controller";
import helper from "./helper";

module.exports = {

    saveProduct : async (productName, priceInCoins, merchantName, merchantEthAddress) => {

        return await marketplaceRepo.insertProduct(productName, priceInCoins, merchantName, merchantEthAddress);

    },

    getAllProducts : async () => {

        const products = await marketplaceRepo.listAllProducts();
   
        let upperProducts =[];
        let lowerProducts = [];

        for (const i in products) {
            if (i < (products.length/2)-1) {
                upperProducts.push(products[i]);
            } else {
                lowerProducts.push(products[i]);
            }
        }

        let result = {
            upperDisplayTray : upperProducts,
            lowerDisplayTray :lowerProducts
        }

        return result;
    },

    checkUserHasCoins : async (userId, coinsRequiredToBuy) => {

        const {accountAddress,tokenBalance} = await userWallet.getWalletDetails(userId);

        console.log(accountAddress,tokenBalance,coinsRequiredToBuy);

        if (+(coinsRequiredToBuy) <= +(tokenBalance)){

            return {status : true, userAddress: accountAddress};

        } else {

            return {status:false};

        }
    },

    sendCoinsToMerchant : async(buyerAddress,coins,merchantAddress,userPassword) => {

        console.log("reaching here",buyerAddress,coins,merchantAddress,userPassword);

        return await helper.sendCoins(buyerAddress,coins,merchantAddress,userPassword);

    }

}