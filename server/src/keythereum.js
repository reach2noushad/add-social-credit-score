import keythereum from 'keythereum';
// import promisefs from 'promise-fs';
import config from './config';
import constants from './constants';

const fs = require('fs-extra')

function importFromFile(address, data) {
    try{
        return keythereum.importFromFile(address, data);
    } catch(e){
        console.log(e);
        throw e;
    }
    
}

function recover(password, keyObject) {
    return keythereum.recover(password, keyObject);
}

function exportToFile(keyObject, datadir) {
    return keythereum.exportToFile(keyObject, datadir);
}

module.exports = {

    generateKeyObject: function (password) {
        console.log(this);
        //Creating keystore
        var params = {
            keyBytes: 32,
            ivBytes: 16
        };
        var dk = keythereum.create(params);
        var kdf = "pbkdf2";
        var options = {
            kdf: "pbkdf2",
            cipher: "aes-128-ctr",
            kdfparams: {
                c: 262144,
                dklen: 32,
                prf: "hmac-sha256"
            }
        };
        return keythereum.dump(password, dk.privateKey, dk.salt, dk.iv, options);
    },
    exportToFile: (keyObject, datadir) => exportToFile(keyObject, datadir),
    recover: (password, keyObject) => recover(password, keyObject),
    importFromFile: (address, data) => importFromFile(address, data),
    getPrivateKey: async (publicKey, password) => {
        console.log(publicKey)
        if (publicKey) {
            console.log("inside here");
            let datadirectory = await constants.datadirectory();
            const keyObject = importFromFile(publicKey, datadirectory.replace("/keystore", ""));
            console.log(keyObject);
            const keyObject_privateKey = recover(password, keyObject);
            const privateKey = "0x" + keyObject_privateKey.toString('hex');
            return privateKey;
        }
        else {
            return {
                error : 404,
                message : "Keystore file unavailable"
            }
        }
    },

    getAddaAccountAndPrivateKey : async function () {

        try {

            const keyFileForQuorumNodeExist = await fs.pathExists('src/key1');
     
            if (keyFileForQuorumNodeExist) {

                const keyJSON = fs.readFileSync('src/key1', 'utf8');
                const keyObj = JSON.parse(keyJSON);
                const password = '';
                const privateKeyHex = keythereum.recover(password, keyObj).toString('hex');
                const privateKey = Buffer.from(privateKeyHex, "hex");
                return {address:"0x"+keyObj.address,privateKey:privateKey};

            } else {

                console.log("Quorum first node (ADDA Account) key file (key1) does not exist")

            }
            
        } catch (error) {

            console.log(error);
            return error;
            
        }

    }
}