


function givePositiveOrNegativeSignForScore(score) {

    if(score.startsWith("-")){
        return score;
    }else {
        return "+"+score;
    }

}

function convertMonthDigitToWords(digit) {

    let month;

    switch (digit) {
        case 1 :
            month = 'January';
            break;
        case 2 :
            month = 'February';
            break;
        case 3 :
            month = 'March';
            break;
        case 4 :
            month = 'April';
            break;
        case 5 :
            month = 'May';
            break;
        case 6 :
            month = 'June';
            break;
        case 7 :
            month = 'July';
            break;
        case 8 :
            month = 'August';
            break;
        case 9 :
            month = 'September';
            break;
        case 10 :
            month = 'October';
            break;
        case 11 :
            month = 'November';
            break;
        case 12 :
            month = 'December';
            break;
        default:
            break;    

    }

    return month;
}



module.exports = {

    transformResultForFullTransactionView : async (transaction) => {

        let requiredResponse = {};

        if (transaction.transactiontype == 'steps'){
            requiredResponse.type = 'Steps';
            requiredResponse.stepsWalked = transaction.stepcount; 
            requiredResponse.score = "+" + Math.trunc(transaction.score);
        } else if (transaction.transactiontype == 'rta') {

            if(transaction.rtatype == 'publictransport') {

                requiredResponse.type = 'Public Transport';
                requiredResponse.cause = "Use of Hafilat Card";
                requiredResponse.from = transaction.hafilatfrom;
                requiredResponse.to = transaction.hafilatto;
                requiredResponse.score = transaction.score;

            } else {

                requiredResponse.type = 'MAWAQIF';
                requiredResponse.cause = transaction.rtatype;
                requiredResponse.location = transaction.location;
                requiredResponse.time = transaction.time;
                requiredResponse.score = transaction.score;

            }
            
        } else if (transaction.transactiontype == 'socialvolunteer') {

            requiredResponse.type = 'Social Volunteer';

            if(transaction.socialvolunteertype == 'blooddonor'){

                requiredResponse.cause = "Blood Donation";

            } else if (transaction.socialvolunteertype == 'charity') {

                requiredResponse.cause = "Charity";

            }

            requiredResponse.score = transaction.score;


        } else if (transaction.transactiontype == 'incidentreporting' ) {

           
            requiredResponse.type = "Incident Reporting";
            requiredResponse.cause = "Fire";
            requiredResponse.score = transaction.score;

        }  else if (transaction.transactiontype == 'energyuse' ) {

            requiredResponse.type = "Energy Use";
            requiredResponse.month = convertMonthDigitToWords(+(transaction.energymetermonth));
            requiredResponse.use = transaction.energymeterusage + "kWh";
            requiredResponse.score = transaction.score;

        }

        return requiredResponse;


    },

    transformResult : async (transaction) => {

        let requiredResponse = {};

        if (transaction.transactiontype == 'rta') {
            requiredResponse.type = 'MAWAQIF';
            requiredResponse.score= transaction.score;
        }else if (transaction.transactiontype == 'steps'){
            requiredResponse.type = 'Steps';
            requiredResponse.score = "+" + transaction.score;
        } else if (transaction.transactiontype == 'incidentreporting') {
            requiredResponse.type = 'Incident Reporting';
            requiredResponse.score = transaction.score;
        } else if (transaction.transactiontype == 'energyuse') {
            requiredResponse.type = 'Energy Use';
            requiredResponse.score = transaction.score;
        } else {
            requiredResponse.transactiontype = transaction.type;
            requiredResponse.score = transaction.score;
        }

        return requiredResponse;
    },

    transformSingleDayScoreResutsForAll : async(stepScore,stepsWalked,rtaScore,socialVolunteerScore,energyScore,incidentScore) => {

        

        let result = {
            "steps" : [{
                "stepswalked" : stepsWalked,
                "scoreforsteps" : givePositiveOrNegativeSignForScore(stepScore)
            }],
            "DOT" : [{
                "score" : givePositiveOrNegativeSignForScore(rtaScore)
            }],
            "socialvolunteer" : [{
                "score" : givePositiveOrNegativeSignForScore(socialVolunteerScore)
            }],
            "energymeter" : [{
                "score" : givePositiveOrNegativeSignForScore(energyScore)
            }],
            "incidentreporting" : [{
                "score" : givePositiveOrNegativeSignForScore(incidentScore)
            }]
        };

        return result;

    }
}