import express from "express";
// import PATH from "path";
import controller from "./controller";
const router = express.Router();


router.get('/', async (req, res, next) => {

    try {

        const data = req.query;

        if (data.userId) {
            
            const isUserScorePresent = await controller.checkIfUserExistInScoreTable(data.userId);
 
            if(isUserScorePresent){

                const userScore = await controller.getUserScore(data.userId);
                res.status(200).send(userScore);

            } else {

                const userScore = await controller.insertAndGiveNewUserScore(data.userId);
                res.status(200).send(userScore);

            }

        }else{

            res.status(422).send({message : "Invalid Query Parameter"})
        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/transactions', async (req, res, next) => {

    try {
        const data = req.query;

        if(data.userId) {

            const isUserScorePresent = await controller.checkIfUserExistInScoreTransactionTable(data.userId);

            if (isUserScorePresent) {

            const transactions = await controller.getUserFullTransactions(data.userId);
            
            res.status(200).send(transactions);

            } else {

                res.status(200).send({message : "User has no score transactions"})

            }

        } else {

            res.status(422).send({message : "Invalid Query Parameter"})

        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});



router.get('/transactions/limited', async (req, res, next) => {

    try {
        const data = req.query;

        if(data.userId) {

            const isUserScorePresent = await controller.checkIfUserExistInScoreTransactionTable(data.userId);

            if (isUserScorePresent) {

            const transactions = await controller.getUserScoreLastTwoDaysTransactions(data.userId);
            
            res.status(200).send([transactions]);

            } else {

                res.status(200).send({message : "User has no score transactions"})

            }

        } else {

            res.status(422).send({message : "Invalid Query Parameter"})

        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/transactions/today', async (req, res, next) => {

    try {
        const data = req.query;

        if(data.userId) {

            const isUserScorePresent = await controller.checkIfUserExistInScoreTransactionTable(data.userId);

            if (isUserScorePresent) {

            const todaysTransactions = await controller.getUserScoreForTodaysTransactions(data.userId);
       
            const userSocialWelfareStatus =  await controller.getSocialWelfareStatus(data.userId);

            todaysTransactions.socialWelfareStatus = userSocialWelfareStatus;
             
            res.status(200).send(todaysTransactions);

            } else {

                res.status(200).send({
                    "steps": [
                        {
                            "scoreforsteps": "+0"
                        }
                    ],
                    "DOT": [
                        {
                            "score": "+0"
                        }
                    ],
                    "socialvolunteer": [
                        {
                            "score": "+0"
                        }
                    ],
                    "energymeter": [
                        {
                            "score": "+0"
                        }
                    ],
                    "incidentreporting": [
                        {
                            "score": "+0"
                        }
                    ],
                    "socialWelfareStatus" : false
                })

            }

        } else {

            res.status(422).send({message : "Invalid Query Parameter"})

        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/transactions/today/steps', async (req, res, next) => {

    try {
        const data = req.query;

        if(data.userId) {

            const isUserScorePresent = await controller.checkIfUserExistInScoreTransactionTable(data.userId);

            if (isUserScorePresent) {

            const todaysTransactions = await controller.getStepScoreForTodaysTransactions(data.userId);
            
            res.status(200).send(todaysTransactions);

            } else {

                let todaysDate = new Date();

                let result = {
                    "date" : todaysDate.toDateString(),
                    "score" : 0,
                    "stepsWalked" : 0,
                    "caloriesBurned":0
    
                }

                res.status(200).send(result)

            }

        } else {

            res.status(422).send({message : "Invalid Query Parameter"})

        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/transactions/today/rta', async (req, res, next) => {

    try {
        const data = req.query;

        if(data.userId) {

            const isUserScorePresent = await controller.checkIfUserExistInScoreTransactionTable(data.userId);

            if (isUserScorePresent) {

            const todaysTransactions = await controller.getRtaScoreForTodaysTransactions(data.userId);
            
            res.status(200).send(todaysTransactions);

            } else {

                let todaysDate = new Date();

                let result = {

                    "date": todaysDate.toDateString(),
                    "score": "+0",
                    "events":[]
                           
                }

                res.status(200).send(result)

            }

        } else {

            res.status(422).send({message : "Invalid Query Parameter"})

        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/transactions/today/socialvolunteer', async (req, res, next) => {

    try {
        const data = req.query;

        if(data.userId) {

            const isUserScorePresent = await controller.checkIfUserExistInScoreTransactionTable(data.userId);

            if (isUserScorePresent) {

            const todaysTransactions = await controller.getSocialScoreForTodaysTransactions(data.userId);
            
            res.status(200).send(todaysTransactions);

            } else {

                let todaysDate = new Date();

                let result = {

                    "date": todaysDate.toDateString(),
                    "score": "+0",
                    "events":[]
                           
                }

                res.status(200).send(result)

            }

        } else {

            res.status(422).send({message : "Invalid Query Parameter"})

        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/transactions/today/energyuse', async (req, res, next) => {

    try {
        const data = req.query;

        if(data.userId) {

            const isUserScorePresent = await controller.checkIfUserExistInScoreTransactionTable(data.userId);

            if (isUserScorePresent) {

            const todaysTransactions = await controller.getEnergyScoreForThisMonth(data.userId);
           
            res.status(200).send(todaysTransactions);

            } else {

                let todaysDate = new Date();


                let result = {

                    "date": todaysDate.toDateString(),
                    "score": "+0",
                    "month":null
                           
                }
        

                res.status(200).send(result)

            }

        } else {

            res.status(422).send({message : "Invalid Query Parameter"})

        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/transactions/today/reportincident', async (req, res, next) => {

    try {
        const data = req.query;

        if(data.userId) {

            const isUserScorePresent = await controller.checkIfUserExistInScoreTransactionTable(data.userId);

            if (isUserScorePresent) {

            const todaysTransactions = await controller.getIncidentScoreForTodaysTransactions(data.userId);
            
            res.status(200).send(todaysTransactions);

            } else {

                let todaysDate = new Date();

                let result = {

                    "date": todaysDate.toDateString(),
                    "score": "+0",
                    "events":[]
                           
                }

                res.status(200).send(result);

            }

        } else {

            res.status(422).send({message : "Invalid Query Parameter"})

        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;