import DBService from "../services/dbservice";


module.exports = {

    doesUserExist : async (userId) => {
        try{
            let userQuery = 'SELECT * FROM userscore WHERE userId = ?';
            let userQueryResult = await DBService.executeQuery({query: userQuery, values: [userId]});
            if(userQueryResult.length > 0) {
                return true;
            } else {
                return false;
            }
            
        }catch(err){
            throw err;
        }
    },

    insertUserToScoreTable : async (userId) => {
   
        try {

            return new Promise(async (resolve, reject) => {
                try {
                    const values = [userId, 0, 0];
                    const query_params = {
                        table: "userscore",
                        columns: ["userId", "usedscore", "totalscore"],
                        values: [values]
                    };
                    await DBService.insertQuery(query_params);
                    resolve();
                } catch (err) {
                    reject(err);
                }
            });

        }catch(e){
            throw e;
        }

    },

    getUserScore : async (userId) => {

        try{
            let userQuery = 'SELECT * FROM userscore WHERE userId = ?';
            let userQueryResult = await DBService.executeQuery({query: userQuery, values: [userId]});
            if(userQueryResult.length > 0) {
                return userQueryResult;
            } else {

                console.log("error retrieving user score");
            }
            
        }catch(err){
            throw err;
        }

    },

    doesUserExistInScoreTransactions : async (userId) => {

        try{
            let userQuery = 'SELECT * FROM userscoretransactions WHERE userId = ?';
            let userQueryResult = await DBService.executeQuery({query: userQuery, values: [userId]});
            if(userQueryResult.length > 0) {
                return true;
            } else {
                return false;
            }
            
        }catch(err){
            throw err;
        }
        
    },

    getLastTwodaysLimitedTransactions : async(userId,today) => {

        let userTransactionsQuery = `SELECT transactiontype, date, score FROM userscoretransactions WHERE date = (SELECT DISTINCT(DATE) FROM userscoretransactions WHERE userId = ${userId} AND NOT date = '${today}' ORDER BY DATE DESC limit 1 ) OR date = (SELECT DISTINCT(DATE) FROM userscoretransactions WHERE userId = ${userId} AND NOT date = '${today}' ORDER BY DATE DESC limit 1,1) ORDER BY DATE DESC;`;
        let userQueryResult = await DBService.executeQuery({query: userTransactionsQuery});
     
        if (userQueryResult.length > 0) {
            return userQueryResult;
        }
    },

    getTodaysTransactions : async(userId,today) => {

        let todaysTransactionsQuery = 'SELECT * FROM userscoretransactions WHERE userId = ? AND date =?';
        let todaysTransactionsQueryResult = await DBService.executeQuery({query: todaysTransactionsQuery, values: [userId,today]});

         
        if (todaysTransactionsQueryResult.length > 0) {
            return todaysTransactionsQueryResult;
        } else {
            return false;
        }

    },

    getEnergyMeterScore : async (userId,month) => {

        let monthsEnergyMeterQuery = 'SELECT * FROM userscoretransactions WHERE userId = ? AND energymetermonth =?';
        let monthsEnergyMeterQueryResult = await DBService.executeQuery({query: monthsEnergyMeterQuery, values: [userId,month]});
        console.log(monthsEnergyMeterQueryResult);
         
        if (monthsEnergyMeterQueryResult.length > 0) {
            return monthsEnergyMeterQueryResult[0].score;
        } else {
            return 0;
        }

    },

    getFullTransactions : async(userId) => {

        let userFullTransactionQuery = 'SELECT * FROM userscoretransactions WHERE userId = ? ORDER BY date desc';
        let userFullTransactionQueryResult = await DBService.executeQuery({query: userFullTransactionQuery, values: [userId]});
       
        if (userFullTransactionQueryResult.length > 0) {
            return userFullTransactionQueryResult;
        }

    },

    getUserDetails : async (userId) => {
        try{
            let userQuery = 'SELECT * FROM user WHERE userId = ?';
            let userQueryResult = await DBService.executeQuery({query: userQuery, values: [userId]});
            if(userQueryResult.length > 0) {
                return userQueryResult;
            } else {
                return false;
            }
            
        }catch(err){
            throw err;
        }
    },

}