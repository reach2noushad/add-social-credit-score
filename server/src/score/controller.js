import scoreRepo from "./scoreRepo";
import helper from "./helper";
import transactions from "../transactions/quorumtransaction";

module.exports = {

    checkIfUserExistInScoreTable : async(userId) => {

        return await scoreRepo.doesUserExist(userId);

    },

    insertAndGiveNewUserScore : async (userId) => {

        await scoreRepo.insertUserToScoreTable(userId);
        return {currentBalance:0,earnedScore:0,spentScore:0};
    },

    getUserScore : async (userId) => {


        const userScoreResult = await scoreRepo.getUserScore(userId);
        let presentScore = userScoreResult[0].totalscore - userScoreResult[0].usedscore;
        return {currentBalance:Math.trunc(presentScore),earnedScore:Math.trunc(userScoreResult[0].totalscore),spentScore:Math.trunc(userScoreResult[0].usedscore)};

    },

    checkIfUserExistInScoreTransactionTable : async (userId) => {

        return await scoreRepo.doesUserExistInScoreTransactions(userId);
    },

    getUserFullTransactions : async(userId) => {

        const tx = await scoreRepo.getFullTransactions(userId);

        let result = [];


        for (const i in tx) {

            let transformedTransaction = await helper.transformResultForFullTransactionView(tx[i]);

            if(result.hasOwnProperty(tx[i].date.toDateString())){
                result[tx[i].date.toDateString()].push(transformedTransaction);
            } else {
                result[tx[i].date.toDateString()] = [transformedTransaction];
            }
        }

        let response = [];

        for (var key in result){
            let dateObj ={}
            dateObj.date = key;
            dateObj.transactions = result[key];

            response.push(dateObj);

        }

        return response;

    },

    getUserScoreLastTwoDaysTransactions : async (userId) => {

        let todaysDate = new Date();
        let today = todaysDate.toISOString().slice(0, 19).split('T')[0].toString();

        const tx = await scoreRepo.getLastTwodaysLimitedTransactions(userId,today);
        
        let result = {};
        
        for (const i in tx) {

            let transformedTransaction = await helper.transformResult(tx[i]);
            
            if(result.hasOwnProperty(tx[i].date.toDateString())){
                result[tx[i].date.toDateString()].push(transformedTransaction);
            } else {
                result[tx[i].date.toDateString()] = [transformedTransaction];
            }
            
        }

        return result;        

    },

    getUserScoreForTodaysTransactions : async (userId) => {

        let todaysDate = new Date();
        let today = todaysDate.toISOString().slice(0, 19).split('T')[0].toString();

        const tx = await scoreRepo.getTodaysTransactions(userId,today);

        console.log(tx);

        //all except energy meter reading be handles here

        let rtaScore = 0;
        let stepsWalked;
        let stepScore = 0;
        let socialVolunteerScore = 0;
        let incidentReportingScore = 0;
        let energymeterReading = 0;
        

        for (const i in tx) {
           
            if (tx[i].transactiontype == 'rta') {

                rtaScore = rtaScore + +(tx[i].score);

            }

            if (tx[i].transactiontype == 'steps') {

                stepScore = Math.trunc(stepScore + +(tx[i].score));
                stepsWalked = +(tx[i].stepcount);

            }

            if (tx[i].transactiontype == 'socialvolunteer') {

                socialVolunteerScore = socialVolunteerScore + +(tx[i].score);

            }

            if (tx[i].transactiontype == 'incidentreporting') {

                incidentReportingScore = incidentReportingScore + +(tx[i].score);

            }
        }

        //energy meter query

        let previousMonth = todaysDate.getMonth(); // 0 - January, so will return 1 count less which is actually corrrect to get previous energy meter reading
 
        const energymetermonthscore = await scoreRepo.getEnergyMeterScore(userId,previousMonth);
 
        energymeterReading = energymeterReading + +(energymetermonthscore);

        return await helper.transformSingleDayScoreResutsForAll(stepScore.toString(),stepsWalked,rtaScore.toString(),socialVolunteerScore.toString(),energymeterReading.toString(),incidentReportingScore.toString());
    },

    getStepScoreForTodaysTransactions : async (userId) => {

   
        let todaysDate = new Date();
        let today = todaysDate.toISOString().slice(0, 19).split('T')[0].toString();

        // let today = '2019-06-11';

        const tx = await scoreRepo.getTodaysTransactions(userId,today);

        if(tx) {

        let stepsWalked = 0;
        let stepScore = 0;

            for (const i in tx) {

                // console.log(tx[i]);

                if (tx[i].transactiontype == 'steps') {

                    console.log(tx[i]);

                    stepScore = stepScore + +(tx[i].score);
                    stepsWalked = stepsWalked + +(tx[i].stepcount);

                }
        
            }

            //1000 steps would burn 44 calories
            //1 step would burn 44 / 1000 calories

            let caloriesBurned = stepsWalked * (44/1000);
    
            let result = {
                "date" : todaysDate.toDateString(),
                "score" : "+"+stepScore.toFixed(2),
                "stepsWalked" : stepsWalked,
                "caloriesBurned":Math.trunc(caloriesBurned)

            }

            return result;

        } else {
  
            let result = {
                "date" : todaysDate.toDateString(),
                "score" : 0,
                "stepsWalked" : 0,
                "caloriesBurned":0

            }

            return result;
        }

    },

    getRtaScoreForTodaysTransactions : async (userId) => {

        let todaysDate = new Date();
        let today = todaysDate.toISOString().slice(0, 19).split('T')[0].toString();

        // let today = '2019-06-11';

        const tx = await scoreRepo.getTodaysTransactions(userId,today);

        let rtaScore = 0;

        if(tx) {

            let events = [];

            for (const i in tx) {

                if (tx[i].transactiontype == 'rta') {

                    let individualEvent = {};

                    rtaScore = rtaScore + +(tx[i].score);

    
                    if (tx[i].rtatype == 'publictransport' ) {

                        individualEvent.date = tx[i].date.toDateString();
                        individualEvent.type = "Use of Hafilat Card";
                        individualEvent.from = tx[i].hafilatfrom;
                        individualEvent.to = tx[i].hafilatto;
                        individualEvent.score = tx[i].score;

                    } else {

                        individualEvent.date = tx[i].date.toDateString();
                        individualEvent.type = tx[i].rtatype;
                        individualEvent.location = tx[i].location;
                        individualEvent.time = tx[i].time;
                        individualEvent.score = tx[i].score;

                    }

                    events.push(individualEvent);

                }
        
            }

            let result = {

                "date": todaysDate.toDateString(),
                "score": rtaScore,
                "events":events
                       
            }


            return result;

        } else {

            let result = {

                "date": todaysDate.toDateString(),
                "score": "+0",
                "events":[]
                       
            }


            return result;

        }

    },

    getSocialScoreForTodaysTransactions : async (userId) => {

        let todaysDate = new Date();
        let today = todaysDate.toISOString().slice(0, 19).split('T')[0].toString();

        // let today = '2019-06-11';

        const tx = await scoreRepo.getTodaysTransactions(userId,today);

        let socialScore = 0;

        if(tx) {

            let events = [];

            for (const i in tx) {

                if (tx[i].transactiontype == 'socialvolunteer') {

                    let individualEvent = {};

                    socialScore = socialScore + +(tx[i].score);

                    individualEvent.date = tx[i].date.toDateString();

                    if(tx[i].socialvolunteertype == 'blooddonor'){

                        individualEvent.type = "Blood Donation";

                    } else if (tx[i].socialvolunteertype == 'charity'){

                        individualEvent.type = "Charity";

                    }
                    
                    individualEvent.score = tx[i].score;

                  
                    events.push(individualEvent);

                }
        
            }

            let result = {

                "date": todaysDate.toDateString(),
                "score": "+"+socialScore,
                "events":events
                       
            }


            return result;

        } else {

            let result = {

                "date": todaysDate.toDateString(),
                "score": "+0",
                "events":[]
                       
            }


            return result;

        }
    },

    getEnergyScoreForThisMonth : async (userId) => {

        //energy meter query
        let todaysDate = new Date();
        let energymeterScore = 0;
        let previousMonth = todaysDate.getMonth(); // 0 - January, so will return 1 count less which is actually corrrect to get previous energy meter reading
        const energymetermonthscore = await scoreRepo.getEnergyMeterScore(userId,previousMonth);
         
        energymeterScore = energymeterScore + +(energymetermonthscore);

        if (energymeterScore > 0) {

            let months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
            let monthForWhichScoreIsObtainedInNumber = +(previousMonth) -1;
            let monthForWhichScoreIsObtainedInMonth = months[monthForWhichScoreIsObtainedInNumber];
            
            let result = {

            "date": todaysDate.toDateString(),
            "score": "+"+energymeterScore,
            "month":monthForWhichScoreIsObtainedInMonth
                   
            }

            return result;

          } else {

            let result = {

                "date": todaysDate.toDateString(),
                "score": "+"+energymeterScore,
                "month":null
                       
            }
    
            return result;


          }

    },

    getIncidentScoreForTodaysTransactions : async (userId) => {

        let todaysDate = new Date();
        let today = todaysDate.toISOString().slice(0, 19).split('T')[0].toString();

        // let today = '2019-06-11';

        const tx = await scoreRepo.getTodaysTransactions(userId,today);

        console.log(tx);

        let incidentScore = 0;

        if(tx) {

            let events = [];

            for (const i in tx) {

                if (tx[i].transactiontype == 'incidentreporting') {

                    let individualEvent = {};

                    incidentScore = incidentScore + +(tx[i].score);

                    individualEvent.date = tx[i].date.toDateString();
                    individualEvent.type = "Fire";
                    individualEvent.score = tx[i].score;

                  
                    events.push(individualEvent);

                }
        
            }

            let result = {

                "date": todaysDate.toDateString(),
                "score": "+"+incidentScore,
                "events":events
                       
            }


            return result;

        } else {

            let result = {

                "date": todaysDate.toDateString(),
                "score": "+0",
                "events":[]
                       
            }


            return result;

        }
    },

    getSocialWelfareStatus : async (userId) => {

        const userDetails = await scoreRepo.getUserDetails(userId);

        let userEthAddress = "0x"+userDetails[0].ethereumAddress;

        return await transactions.isUserPresentInSocialWelfareStatus(userEthAddress);

    }
}