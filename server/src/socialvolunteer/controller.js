import socialVolunteerRepo from "./socialVolunteerRepo";

module.exports = {

    addSocialVolunteerToUserTransaction: async(userId,type) => {

        let socialVolunteerScore = 100;

        let todaysDate = new Date();
        let today = todaysDate.toISOString().slice(0, 19).split('T')[0];

        let socialVolunteerType = type.toLowerCase();
   
        await socialVolunteerRepo.addSocialVolunteerScore(userId,today,socialVolunteerType,"+"+socialVolunteerScore);

        const presentUserTotalScore = await socialVolunteerRepo.getUserTotalScore(userId);

        let addedScore = +(presentUserTotalScore) + +(socialVolunteerScore);

        await socialVolunteerRepo.updateUserTotalScore(userId,addedScore);

    }
}