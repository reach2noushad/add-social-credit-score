import DBService from "../services/dbservice";

module.exports = {

    addSocialVolunteerScore : (userId,today,socialVolunteerType,score) => {

        return new Promise(async (resolve, reject) => {
            try {
                const values = [userId,'socialvolunteer', today, socialVolunteerType,score];
                const query_params = {
                    table: "userscoretransactions",
                    columns: ["userId", "transactiontype", "date","socialvolunteertype","score"],
                    values: [values]
                };
                await DBService.insertQuery(query_params);
                resolve();
            } catch (err) {
                reject(err);
            }
        });

    },


    getUserTotalScore : async (userId) => {

        let userScoreQuery = 'SELECT * FROM userscore WHERE userId = ?';
        let userScoreQueryResult = await DBService.executeQuery({query: userScoreQuery, values: [userId]});
  
        if (userScoreQueryResult.length > 0) {
            return userScoreQueryResult[0].totalscore;
        }

    },

    updateUserTotalScore :  async (userId, score) => {


        return new Promise(async (resolve, reject) => {
            try {
                const query_params = {
                    table: "userscore",
                    set: {
                        totalscore: score
                    },
                    where: {
                        userId: userId
                    }
                };
    
                await DBService.updateQuery(query_params);
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }


}