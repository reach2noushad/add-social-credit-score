import express from "express";
import controller from "./controller";
const router = express.Router();


router.post('/', async (req, res, next) => {

    try {

        const data = req.body;

        console.log(data);

        if (data.userId && data.type && data.type == "charity" || data.type == "blooddonor") {

            await controller.addSocialVolunteerToUserTransaction(data.userId,data.type);

            res.status(200).send({message:"success"})

               
        }else{

            res.status(422).send({message : "Invalid Input Parameters"})
        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});





module.exports = router;