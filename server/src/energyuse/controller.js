import energyRepo from "./energyRepo";

module.exports = {

    checkIfuserHasAlreadyUploadedForMonth: async (userId,month) => {

        return await energyRepo.checkIfUserHasMonthAlreadyInDB(userId,month);

    },

    saveEnergyUse : async (userId,consumedUnits,month) => {

        let previousMonthHavingRecord = false;
        let decreaseInUsageStatus = false;
        let decreasedUse;

        const {status,previousMonthUsage} = await energyRepo.getPreviousMonthUsage(userId,month);

        let score = 0;

        console.log(status);

        if (status) {
            //previous month usage exist
            previousMonthHavingRecord = true;

            if (consumedUnits < previousMonthUsage) {

                decreaseInUsageStatus = true;
                decreasedUse = +(previousMonthUsage) - +(consumedUnits);

                score = score + 100;
                
            } 

        }

        //update main user score

        const presentUserTotalScore = await energyRepo.getUserTotalScore(userId);
        let newScore = +(presentUserTotalScore) + +(score);
        await energyRepo.updateUserTotalScore(userId,newScore);

        let todaysDate = new Date();
        let today = todaysDate.toISOString().slice(0, 19).split('T')[0];

        score = "+"+score;

        await energyRepo.saveEnergyUse(userId,today,month,consumedUnits,score);

        let result = {
            "previousMonthHavingRecord":previousMonthHavingRecord,
            "decreaseInUsageStatus" : decreaseInUsageStatus,
            "decreasedConsumption": decreasedUse,
            "scoreRewarded":score
        }

        return result;


    }
}