module.exports = {

    convertMonthInWordsToDigits : async (month) => {

        let digit;

        switch(month){

            case 'January':
                digit = 1;
                break;
            case 'February':
                digit = 2;
                break;
            case 'March':
                digit = 3;
                break;
            case 'April':
                digit = 4;
                break;
            case  'May':
                digit = 5;
                break;
            case 'June':
                digit = 6;
                break;
            case 'July':
                digit = 7;
                break;
            case 'August':
                digit = 8;
                break;
            case 'September':
                digit = 9;
                break;
            case 'October':
                digit = 10;
                break;
            case 'November':
                digit = 11;
                break;
            case 'December':
                digit = 12;
                break;
            default:
                digit = 0;
                break;    
        }

        return digit;
    }
}

