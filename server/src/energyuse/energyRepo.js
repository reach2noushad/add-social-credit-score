import DBService from "../services/dbservice";


module.exports = {

    checkIfUserHasMonthAlreadyInDB : async(userId,month) => {

        let userEnergyQuery = 'SELECT * FROM userscoretransactions WHERE userId = ? AND energymetermonth =?';
        let userEnergyQueryResult = await DBService.executeQuery({query: userEnergyQuery, values: [userId,month]});
  
        if (userEnergyQueryResult.length > 0) {
            return true;
        } else {
            return false;
        }

    },

    getPreviousMonthUsage : async (userId,presentmonth) => {

        let previousMonthinDigit = +(presentmonth) - 1;

        let userPreviousMonthEnergyQuery = 'SELECT * FROM userscoretransactions WHERE userId = ? AND energymetermonth =? AND transactiontype="energyuse"';
        let userPreviousMonthEnergyQueryResult = await DBService.executeQuery({query: userPreviousMonthEnergyQuery, values: [userId,previousMonthinDigit]});
        
        console.log(userPreviousMonthEnergyQueryResult);

        if (userPreviousMonthEnergyQueryResult.length >0) {
            console.log("inside");
            return {status:true,previousMonthUsage:userPreviousMonthEnergyQueryResult[0].energymeterusage};
        }else{
            return {status:false};
        }
    },

    saveEnergyUse : async (userId,date,month,consumedUnits,score) => {

        try {

            return new Promise(async (resolve, reject) => {
                try {
                    const values = [userId,"energyuse", date, month, consumedUnits, score ];
                    const query_params = {
                        table: "userscoretransactions",
                        columns: ["userId","transactiontype", "date", "energymetermonth", "energymeterusage","score"],
                        values: [values]
                    };
                    await DBService.insertQuery(query_params);
                    resolve();
                } catch (err) {
                    reject(err);
                }
            });

        }catch(e){
            throw e;
        }
    },

    getUserTotalScore : async (userId) => {

        let userScoreQuery = 'SELECT * FROM userscore WHERE userId = ?';
        let userScoreQueryResult = await DBService.executeQuery({query: userScoreQuery, values: [userId]});
  
        if (userScoreQueryResult.length > 0) {
            return userScoreQueryResult[0].totalscore;
        }

    },

    updateUserTotalScore :  async (userId, score) => {


        return new Promise(async (resolve, reject) => {
            try {
                const query_params = {
                    table: "userscore",
                    set: {
                        totalscore: score
                    },
                    where: {
                        userId: userId
                    }
                };
    
                await DBService.updateQuery(query_params);
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

}