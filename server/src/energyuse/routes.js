import express from "express";
import helper from "./helper";
import controller from "./controller";
const router = express.Router();


router.post('/', async (req, res, next) => {

    try {

        
        const data = req.body;

        console.log(data);

        if (data.userId && data.month && data.consumedUnits) {

            const monthInDigits = await helper.convertMonthInWordsToDigits(data.month);
           
            if (monthInDigits == 0) {

                res.status(200).send({message:"Wrong Input Month"});

            } else {

                const isMonthHavingData = await controller.checkIfuserHasAlreadyUploadedForMonth(data.userId,monthInDigits);

                if(isMonthHavingData){
                    res.status(200).send({message:"Bill already exist for the Given Month"});
                } else {

                    const result = await controller.saveEnergyUse(data.userId,data.consumedUnits,monthInDigits);

                    res.send(result);

                }
            }


        }else{

            res.status(422).send({message : "Invalid Request Parameter"})
        }

    }
    catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;