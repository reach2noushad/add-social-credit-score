const os = require('os');
const keythereum = require('./keythereum');

const prefix = os.homedir();



//exports.ethersLoadAccountPrivateKey = '0xF6F73ED9A00412658BFD82BCBEE2435764D97AD3B3C2C9FA5CB3B6CDF9B50C30';
//Account is 0x3C0B8Cb523304079e76132544c282a3D86Ebc0F7 (Rinkeby) if needed to reload

module.exports = {

    db : {
        host: "localhost",
        user: "root",
        password: "attinad@123",
        database: "adda"
    },

    datadir : prefix + '/addaKeystore/keystore',

    port : 2122 ,

    ethersLoadAccountPrivateKey : '0xF6F73ED9A00412658BFD82BCBEE2435764D97AD3B3C2C9FA5CB3B6CDF9B50C30',

    addaAccountAddress : "0x3C0B8Cb523304079e76132544c282a3D86Ebc0F7",

    tokenContractDeployedAddress : '0x1932c48b2bf8102ba33b4a6b545c32236e342f34',

    tokenContractAbi : [{"constant":false,"inputs":[],"name":"acceptOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"spender","type":"address"},{"name":"tokens","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"spender","type":"address"},{"name":"tokens","type":"uint256"},{"name":"data","type":"bytes"}],"name":"approveAndCall","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_amount","type":"uint256"}],"name":"mint","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"to","type":"address"},{"name":"tokens","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"tokenAddress","type":"address"},{"name":"tokens","type":"uint256"}],"name":"transferAnyERC20Token","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"from","type":"address"},{"name":"to","type":"address"},{"name":"tokens","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"amount","type":"uint256"}],"name":"Mint","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"tokens","type":"uint256"},{"indexed":false,"name":"time","type":"uint256"}],"name":"tokenTransfers","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"tokens","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"tokenOwner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"tokens","type":"uint256"}],"name":"Approval","type":"event"},{"constant":true,"inputs":[{"name":"tokenOwner","type":"address"},{"name":"spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"tokenOwner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"donationAccount","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"newOwner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"socialWelfareEligibiltyBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"socialWelfareFundStatus","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"}],

    socialWelfareAccountEthAddress : "0x1519cf577bcb534a9a66419b605d210b71a1cabc",

    events : [
        {
            tokenTransfers : "0xf2ed1f49a38f3061d14ce162e3da10d19b8331c8219a6723cbc1a3193a3c93e1"
        } 
    ],

    ethereumAccountNames : [
        {
            "0xed9d02e382b34818e88B88a309c7fe71E65f419d": "FED",
            "0x9f13ab67b7ef43a74471f02d9cb3d57854669002" : "Ahamed",
            "0x4aed8d9a28ee9be1a3b0e56c2d1c991c2f1fde3e" : "Ibrahim",
            "0xfd8aB7dAf6a8D3e733A058b5efF0B6c9fcFe5B8c" :  "Lulu Hypermarket",
            "0xa0056e9b86dF627Cb089d7E2B76AB8CC4804f516" : "Carrefour Hypermarket",
            "0x58b02e608644479f33a5d595c99e584b7e5d0674" : "Mohamed",
            "0x2cc1dc155fa0b345a1f3110bc5df171c6ebcc545" : "Ismail",
            "0x1d389ede8515bf1b3965466b265a1c8ff5be8700" : "Sajeer Hasan",
            "0x05E2980bA36c4F805Da1811bABf661b829eD9D2b" : "Dubai Police",
            "0x51fda29cbebae0e188899dfb435bcbe424a5c04f" : "Abu Dhabi Traffic Dept",
            "0x1519Cf577BCB534A9A66419b605d210B71A1cabc" : "Social Welfare Account",
            "0xf91480014884b52e48646a34cb2c0952924ff991" : "Arshad Ahamed",
            "0xd3177b3ecbf9bcf44808b62fd1cc04543d1f7538" : "Imtiyaaz",
            "0x0d175dd02dd6c8d23a4a9d373f9f693946ce5b05" : "Khalid",
            "0x98896e5385CB997aaed15174c899f296F5408163" : "Ahmed Ilyas"
        
        }
    ],

    merchantAddressList : ["0xfd8ab7daf6a8d3e733a058b5eff0b6c9fcfe5b8c","0xa0056e9b86df627cb089d7e2b76ab8cc4804f516"],

    stepToScoreValue : 10,

    minimumScoreRequiredToRedeemCoin : 1000,

    scoreToCoinRatio : {score : 1000, coin : 100},

    // providerURL : "http://127.0.0.1",

    providerURL : "http://192.168.3.13",

    providerPort : "22000"

    
}

