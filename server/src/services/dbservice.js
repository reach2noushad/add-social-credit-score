import mysql from '../mysql';


module.exports = {

    executeQuery : ({query, values}) => {
        console.log(query,values);
        return mysql.query(query, values);
    },

    insertQuery : (params) => {
        const table = params.table,
            columns = params.columns,
            values = params.values,
            query = `INSERT INTO ${table}(${columns.join(',')}) VALUES ?`;
        return mysql.query(query, [values]);
    },

    updateQuery: (params, dialect) => {
        let set_params = [],
            where_params = [];
        const table = params.table,
            setColumns = params.set,
            whereColumns = params.where;
        for (var key in setColumns) {
            set_params.push(`${key} = "${setColumns[key]}"`);
        }
        for (var key in whereColumns) {
            where_params.push(`${key} = "${whereColumns[key]}"`);
        }
        let condition = '';
        if(!Array.isArray(dialect)) {
            condition = where_params.join(' ' + dialect + ' ');
        }
        else if(dialect.length === where_params.length - 1) {
            dialect.forEach((d,i) => {
                if(condition) condition = condition + " " + d + " " + where_params[i+1];
                else condition = " " + where_params[i] + " " + d + " " + where_params[i+1];
            });
        }
        else {
            console.log(where_params);
            condition = where_params[0];
        }
        const query = `UPDATE ${table} SET ${set_params.join(',')} WHERE ${condition}`;
        console.log("query", query);
        return mysql.query(query);
    }

}