import socialWelfareRepo from "./socialWelfareRepo";
import config from "../config";
import transaction from "../transactions/quorumtransaction";
import keythereum from '../keythereum';

module.exports = {

    getUserEthereumAddress : async (userId) => {

        const userDetails = await socialWelfareRepo.getUser(userId);
        if(userDetails){
            let hexifiedEthAddress = "0x"+userDetails[0].ethereumAddress;
            return hexifiedEthAddress;
        } else {
            return null;
        }
    },

    donateCoins : async (userAddress,password,coins) => {

        const socialWelfareAccountAddress = config.socialWelfareAccountEthAddress;

        let decimalCompensatedCoins = +(coins) * 100; //2 decimal places

        const fromAddressPrivateKey = await keythereum.getPrivateKey(userAddress,password);

        const transactionResult = await transaction.sendTokens(userAddress,fromAddressPrivateKey,decimalCompensatedCoins,socialWelfareAccountAddress);

        if(transactionResult.status == 200) {
            return true;
        } else {
            return false;
        }

    }

}