import DBService from "../services/dbservice";

module.exports = {

    getUser : async (userId)  => {

    let userQuery = 'SELECT * FROM user WHERE userId = ?';
    let userResult = await DBService.executeQuery({query: userQuery, values: [userId]});

    if(userResult.length > 0) {
        return userResult;
        } else {
        console.log("user not present in db");
        return null;
    }
}

}