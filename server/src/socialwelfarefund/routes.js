import express from "express";
import config from "../config";
import wallet from "../wallet/wallet";
import controller from "./controller";
const router = express.Router();


router.get('/balance', async (req, res, next) => {

    try {

        const socialWelfareAccountAddress = config.socialWelfareAccountEthAddress;
        const balance = await wallet.getTokenBalance(socialWelfareAccountAddress);

        res.status(200).send({balance:balance/100});

    }

    catch (e) {
        res.status(500).send(e);
    }
});


router.post('/donate', async (req, res, next) => {

    try {

        const data = req.body;

        if(data.userId && data.coins && data.password){

            const userEthAddress = await controller.getUserEthereumAddress(data.userId);

            if(userEthAddress){

                const result = await controller.donateCoins(userEthAddress,data.password,data.coins);

                if (result) {
                    
                    res.status(200).send({message: "You have successfully donated "+data.coins+" coins to Social Welfare Fund"});

                } else {

                    res.status(500).send({message : "Something Went Wrong with the Transaction, Please Try Again!"})

                }

            }else{

                res.status(422).send({message : "No user with the given id exist"})

            }

        } else {

            res.status(422).send({message : "Invalid Input Parameters"})

        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});


module.exports = router;