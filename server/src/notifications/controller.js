import userHelper from "../user/helper";
import notificationsRepo from "./notificationsRepo";
import helper from "./helper";
import { socialWelfareAccountEthAddress } from "../config";


module.exports = {

    checkAndNotifyUser : async(userId, userEthAddress) => {

        const userSocialWelfareStatus = await userHelper.getUserSocialWelfareStatus(userEthAddress);
        
        console.log(userSocialWelfareStatus);

        if (userSocialWelfareStatus) {

            console.log("user is eligible, please notify the android app");

            const userNotificationStatus = await notificationsRepo.checkUserNotificationStatus(userId);

            if(userNotificationStatus) {

                console.log("user already notified");

            } else {

                console.log("user not notified");

                await notificationsRepo.updateUserNotificationStatus(userId);    

                const userDeviceTokens = await notificationsRepo.getUserDeviceKeys(userId);

                let registartionIds = [];

                if (userDeviceTokens.status) {

                    userDeviceTokens.result.forEach(element => {

                        registartionIds.push(element.devicetoken);
                        
                    });


                    await helper.sendNotificationToAndroidApp(registartionIds);

      
                } else {
                    console.log("user does not have device tokens to notify");
                }

            }

        }

    }
}



