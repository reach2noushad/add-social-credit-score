import express from "express";
import controller from "./controller";
import notificationsRepo from "./notificationsRepo";
const router = express.Router();



router.post('/addDeviceKey', async (req, res, next) => {

    try {

        let data = req.body;

        if (data.userId && data.deviceToken) {

            const result = await notificationsRepo.checkIfUserAlreadyHasKey(data.userId,data.deviceToken);

            if (result.status) {

                res.status(200).send({message: "User Already Has Device Token"}); 


            } else {

                await notificationsRepo.insertDeviceKey(data.userId,data.deviceToken);

                res.status(200).send({message: "Success"});  

            }
          

        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});

//check api for notification
// router.get('/', async (req, res, next) => {

//     console.log("calling notifications api");

//     try {

//         await controller.checkAndNotifyUser("26","f91480014884b52e48646a34cb2c0952924ff991");

    
//     }
//     catch (e) {
//         res.status(500).send(e);
//     }
// });


module.exports = router;