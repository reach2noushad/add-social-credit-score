import DBService from "../services/dbservice";


module.exports = {

    insertDeviceKey : async(userId,deviceKey) => {

        try {

            return new Promise(async (resolve, reject) => {
                try {
                    const values = [userId, deviceKey];
                    const query_params = {
                        table: "userdevicetokens",
                        columns: ["userId", "devicetoken"],
                        values: [values]
                    };
                    await DBService.insertQuery(query_params);
                    resolve();
                } catch (err) {
                    reject(err);
                }
            });

        }catch(e){
            throw e;
        }
    },

    getUserDeviceKeys : async(userId) => {

        let userDeviceKeysQuery = 'SELECT * FROM userdevicetokens WHERE userId = ?';
        let userDeviceKeysQueryResult = await DBService.executeQuery({query: userDeviceKeysQuery, values: [userId]});

        if(userDeviceKeysQueryResult.length > 0) {
            return {status:true, result :userDeviceKeysQueryResult};
        } else {
            return {status:false};
        }


    },

    checkIfUserAlreadyHasKey : async (userId,deviceKey) => {

        let query = `SELECT * FROM userdevicetokens WHERE userId = '${userId}' AND devicetoken ='${deviceKey}'`;
        let result = await DBService.executeQuery({query: query});
            
        if(result.length > 0) {
            return {status:true};
        } else {
            return {status:false};
        }

    },

    checkUserNotificationStatus : async (userId) => {

        let query = `SELECT userNotificationStatus FROM user WHERE userId = '${userId}'`;
        let result = await DBService.executeQuery({query: query});

            
        if(result.length > 0) {
            if(result[0].userNotificationStatus == 'true'){
                return true;
            } else {
                return false;
            }
            
        } 

    },

    updateUserNotificationStatus : async (userId) => {

        return new Promise(async (resolve, reject) => {
            try {
                const query_params = {
                    table: "user",
                    set: {
                        userNotificationStatus: 'true'
                    },
                    where: {
                        userId: userId
                    }
                };
    
                await DBService.updateQuery(query_params);
                resolve();
            } catch (err) {
                reject(err);
            }

        });


    }

}