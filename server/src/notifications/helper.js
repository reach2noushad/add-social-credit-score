

const rp = require('request-promise');

module.exports = {

    sendNotificationToAndroidApp : (registrationIds) => {

        var options = {
            method: 'POST',
            uri: 'https://fcm.googleapis.com/fcm/send',
            headers: {
                'Authorization' : 'key=AAAAg-aUufs:APA91bHqzfx4Y-vUON4X8olqn144SeX3SNRpoXQjY7L_tBg77CxsBRU3bs9nKXek48ginBvLarviScL3W7mqax1tAiAFmd4X8nKSgGtb0znbhQ1vN5j9XKZS_kJ91gzRMuclbXX3yxGK'
            },
            body: { 
                "notification" : {
                    "body" : "Good News!",
                    "title": "You are now enrolled to Social Welfare Fund!"
                },
               
                  "registration_ids":registrationIds
                },
            json: true 
        };
         
        rp(options)
            .then(function (parsedBody) {
                console.log("Notification Send");
              })
            .catch(function (err) {
                console.log("error sending notification");
            });
    }

}