const PATH = require('path');
const config = require('./config');

const mysql = require('./mysql');
var mkdirp = require('mkdirp');
var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var app = express();
const routes = require('./routes');
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))


const log = require(PATH.resolve('src/log.js'));

process.on('uncaughtException', function (err) {
  log.error(err);
  process.exit(1);
});

process.on('unhandledRejection', (err) => {
  log.error(err);
  process.exit(1);
});

app.use('/api', routes);

initializeServer();

function initializeServer() {
  mkdirp(config.datadir, function (err) {
    if (err) {
      console.error(err);
      res.status(500).send(err);
    }
    else {
      console.log('KeyStore Directory Initialised');
      mysql.createDBConnection()
        .then(res => {
          app.listen(config.port, function () {
            console.log("App listening on port",config.port);
          });
        })
        .catch(e => log.error(e));
    }
  });
}