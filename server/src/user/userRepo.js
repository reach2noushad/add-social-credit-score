import DBService from "../services/dbservice";

async function doesUserExist(emiratesId) {
    try{
        let emiratesIdQuery = 'SELECT emiratesId FROM user WHERE emiratesId = ?';
        let emiratesIdResult = await DBService.executeQuery({query: emiratesIdQuery, values: [emiratesId]});
        if(emiratesIdResult.length > 0) {
            return true;
        } else {
            return false;
        }
        
    }catch(err){
        throw err;
    }
}

async function insertUser (emiratesId, secretPassword, fullName, mobileNumber, ethereumAddress) {
  
    return new Promise(async (resolve, reject) => {
        try {
            const values = [emiratesId, secretPassword, fullName, mobileNumber, ethereumAddress];
            const query_params = {
                table: "user",
                columns: ["emiratesId", "password", "name","mobileNumber","ethereumAddress"],
                values: [values]
            };
            await DBService.insertQuery(query_params);
            resolve();
        } catch (err) {
            reject(err);
        }
    });
}

async function getUserDetails(mobileNumber) {

    try{

        let userQuery = 'SELECT * FROM user WHERE mobileNumber = ?';
        let userResult = await DBService.executeQuery({query: userQuery, values: [mobileNumber]});
     
        if(userResult.length > 0) {
            return userResult;
        } else {
            console.log("user not present in db");
        }
       
    }catch(err){
        throw err;
    }
}

async function getUser(userId) {

    let userQuery = 'SELECT * FROM user WHERE userId = ?';
    let userResult = await DBService.executeQuery({query: userQuery, values: [userId]});
    console.log(userResult);
    if(userResult.length > 0) {
        return userResult;
    } else {
        console.log("user not present in db");
        return null;
    }


}

// async function getUserWithSSN(ssn) {
//     try{
//         let userQuery = 'SELECT users.globalID, Account AS account, name FROM Correlation AS cor JOIN users ON cor.globalID = users.globalID WHERE userID = ?';
//         let userResult = await DBService.executeQuery({query: userQuery, values: [ssn]});
//         return userResult[0];
//     }catch(err){
//         throw err;
//     }
// }

// async function getGlobalId  (ssn) {
//     try{
//         let globalIdQuery = 'SELECT globalID FROM Correlation WHERE userID = ?';
//         let globalIdResult = await DBService.executeQuery({query: globalIdQuery, values: [ssn]});
//         return globalIdResult[0];
//     }catch(err){
//         throw err;
//     }
// }

// async function getAccountAddress (globalId)  {
//     try {
//         let accountQuery = 'SELECT Account FROM users WHERE globalID = ?';
//         let accountIdResult = await DBService.executeQuery({query: accountQuery, values: [globalId]});
//         return accountIdResult[0];
//     } catch (err) {
//         throw err;
//     }
// }

// async function getEmrUniqueId(globalId, provider)  {
//     try {
//         let emrUniqueIdQuery = 'SELECT cipher AS emrUniqueId,emrOrder FROM MultipleUUID WHERE globalID = ? AND system = ?';
//         let emrUniqueIdResult = await DBService.executeQuery({query: emrUniqueIdQuery, values: [globalId, provider]});
//         return emrUniqueIdResult[0];
//     } catch (err) {
//         throw err;
//     }
// }

// async function getUserCypherText(globalId)  {
//     try {
//         let emrUniqueIdQuery = 'SELECT cipher  FROM MultipleUUID WHERE globalID = ? AND emrOrder = ?';
//         let emrUniqueIdResult = await DBService.executeQuery({query: emrUniqueIdQuery, values: [globalId, 1]});
//         return emrUniqueIdResult[0];
//     } catch (err) {
//         throw err;
//     }
// }

// async function insertCorrelationMapping (ssn, globalId) {
//     return new Promise(async (resolve, reject) => {
//         try {
//             let correlationMappingParams = {
//                 table: "Correlation",
//                 columns: ["userID", "globalID"],
//                 values: [[ssn, globalId]]
//             };
//             await DBService.insertQuery(correlationMappingParams);
//             resolve();
//         } catch (err) {
//             reject(err);
//         }
//     });
// }

// async function insertUser (globalId, secret, fullName) {
//     return new Promise(async (resolve, reject) => {
//         try {
//             const values = [globalId, secret, fullName];
//             const query_params = {
//                 table: "users",
//                 columns: ["globalId", "otp", "name"],
//                 values: [values]
//             };
//             await DBService.insertQuery(query_params);
//             resolve();
//         } catch (err) {
//             reject(err);
//         }
//     });
// }

// async function updateUserOtp (globalId, secret) {
//     return new Promise(async (resolve, reject) => {
//         try {
//             const query_params = {
//                 table: "users",
//                 set: {
//                     otp: secret
//                 },
//                 where: {
//                     globalId: globalId
//                 }
//             };

//             await DBService.updateQuery(query_params);
//             resolve();
//         } catch (err) {
//             reject(err);
//         }
//     });
// }

// async function updateUserAccount (globalId, accountAddress) {
//     return new Promise(async (resolve, reject) => {
//         try {
//             const query_params = {
//                 table: "users",
//                 set: {
//                     Account: accountAddress,
//                     otp: 'NULL'
//                 },
//                 where: {
//                     globalId: globalId
//                 }
//             };
//             await DBService.updateQuery(query_params, "AND");

//             resolve();
//         } catch (err) {
//             reject(err);
//         }
//     });
// }

// async function insertEmrUserMapping (globalId, provider, encryptedUUID) {
//     return new Promise(async (resolve, reject) => {
//         try {
//             let emrUserMappingQuery = "SELECT max(emrOrder) AS lastOrder  FROM MultipleUUID WHERE globalID= ?";
//             let emrOrderResult = await  DBService.executeQuery({query: emrUserMappingQuery, values: [globalId]});
//             let emrOrder = emrOrderResult[0].lastOrder || 0;
//             const emrUserMappingParams = {
//                 table: "MultipleUUID",
//                 columns: ["globalID", "emrOrder", "system", "cipher"],
//                 values: [[globalId, emrOrder + 1, provider, encryptedUUID]]
//             };
//             await DBService.insertQuery(emrUserMappingParams);
//             resolve();
//         } catch (err) {
//             reject(err);
//         }
//     });
// }

// async function getUsersWithAccounts(accounts) {
//     try{
//         accounts = Array.from(new Set(accounts)); // to remove duplicate items
//         let accountAddress = '';

//         accountAddress += ' (';
//         for (let account of accounts) {
//             accountAddress += `'${account.substr(2)}'`;
//             (accounts.indexOf(account) < accounts.length - 1) ?  accountAddress += ',' :  accountAddress += ')';
//         }

//         let userQuery = `SELECT users.globalID, Account AS account, name, cor.userID as Adhaar FROM Correlation AS cor JOIN users ON cor.globalID = users.globalID WHERE users.Account IN ${accountAddress}`;
//         let userResult = await DBService.executeQuery({query: userQuery, values: []});
//         return userResult;

//     }catch(err){
//         throw err;
//     }
// }

module.exports = {
    doesUserExist,
    insertUser,
    getUserDetails,
    getUser
};