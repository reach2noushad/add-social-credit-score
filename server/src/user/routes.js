import express from "express";
import controller from "./controller";
const router = express.Router();

router.post('/register', async (req, res, next) => {

    try {

        const data = req.body;
 
        if(data.eId && data.name && data.mobileNo && data.password) {

        const userStatus = await controller.isUserRegistered(data.eId);
            if(userStatus) {
                res.status(409).send({message: "User already exists"});
            } else {
         
                await controller.onboardUser(data);
                res.status(200).send({message : "User Registered Successfully"})
            }
        } else {
     
            res.status(422).send({message : "Invalid Input"})
        }

    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.post('/login', async (req, res, next) => {

    try {

        const data = req.body;
 
        if(data.mobileNo && data.password) {

            const {status,userId,userName,socialWelfareEnrollmentStatus} = await controller.loginUser(data);
       
            if(status) {

                res.status(200).send({
                    message: "User Logged In Successfully",
                    userId : userId, 
                    name : userName, 
                    isUserInSocialWelfare:socialWelfareEnrollmentStatus,
                    address : {
                        "Address1" : "Flat No 305",
                        "Address2" : "BelRasheed Building",
                        "PO" : "PO BOX 3546",
                        "State" : "Abu Dhabi",
                        "Country": "UAE" 
                    }
                             
                });
            } else {
                res.status(400).send({message: "Invalid Credentials"});
            }

        } else {
     
            res.status(422).send({message : "Invalid Input"})
        }

    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/wallet', async (req, res, next) => {

    try {
        const data = req.query;

        if(data.userId) {
            
            const walletDetails = await controller.getWalletDetails(data.userId);
            res.status(200).send(walletDetails);

        } else {

            res.status(422).send({message : "Invalid Query Parameter"})

        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});



module.exports = router;