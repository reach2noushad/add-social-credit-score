import keythereum from '../keythereum';
import config from '../config';
import wallet from '../wallet/wallet';
import transaction from "../transactions/quorumtransaction";



module.exports = {

    createWalletForUser : async (password) => {
        const keyObject = await keythereum.generateKeyObject(password);
        await keythereum.exportToFile(keyObject,config.datadir);
        return keyObject.address;
    },

    getTokenBalance : async (ethereumAddress) => {

        const balance = await wallet.getTokenBalance(ethereumAddress);
        return +(balance)/100;

    },

    getUserSocialWelfareStatus : async(ethereumAddress) => {

        let hexifiedAddress = "0x"+ethereumAddress;
        const result = await transaction.isUserPresentInSocialWelfareStatus(hexifiedAddress);
        return result;
    }
}