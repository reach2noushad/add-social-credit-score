import DBService from '../services/dbservice';
import config from '../config';
import userRepo from "./userRepo";
import sha from "sha1";
import helper from "./helper";
import transaction from "../transactions/quorumtransaction"



module.exports = {

    isUserRegistered : async (emiratesId) => {
        return await userRepo.doesUserExist(emiratesId);
    },

    onboardUser : async (data) => {
        let emiratesId = data.eId;
        let secretPassword = sha(data.password);
        let fullName  = data.name;
        let mobileNumber = data.mobileNo;
        const ethereumAddress  = await helper.createWalletForUser(data.password);
        let hexifyedAccountAddress = "0x"+ethereumAddress;
        //commenting as there is no need of value ethers in quorum
        //transaction.transferEthers(hexifyedAccountAddress); //use it when to load ethers at background
        return await userRepo.insertUser(emiratesId,secretPassword,fullName,mobileNumber,ethereumAddress);
    },


    loginUser : async(data) => {
        const userDetails = await userRepo.getUserDetails(data.mobileNo);
        if(userDetails[0].password == sha(data.password)){
            const socialWelfareStatus = await helper.getUserSocialWelfareStatus(userDetails[0].ethereumAddress);
            return {status:true,userId:userDetails[0].userId, userName : userDetails[0].name, socialWelfareEnrollmentStatus:socialWelfareStatus};
        } else {
            return {status:false};
        }
    },

    getWalletDetails : async(userId) => {

        const userDetails = await userRepo.getUser(userId);
        const ethereumAddress = userDetails[0].ethereumAddress;
        let hexifyedAccountAddress = "0x"+ethereumAddress;
        let userTokenBalance = await helper.getTokenBalance(hexifyedAccountAddress);
        return {accountAddress:hexifyedAccountAddress,tokenBalance:userTokenBalance};


    }


}

