import incidentRepo from "./incidentRepo";

module.exports = {

    saveFireIncident : async (userId) => {

        let score = 100;

        let todaysDate = new Date();
        let today = todaysDate.toISOString().slice(0, 19).split('T')[0];

        await incidentRepo.saveFireIncident(userId,today,score);

        const presentUserTotalScore = await incidentRepo.getUserTotalScore(userId);
        let newScore = +(presentUserTotalScore) + +(score);
        await incidentRepo.updateUserTotalScore(userId,newScore);

    }
}