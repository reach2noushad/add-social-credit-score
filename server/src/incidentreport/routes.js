import express from "express";
import controller from "./controller";
const router = express.Router();


router.post('/', async (req, res, next) => {

    try {

        const data = req.body;

        if (data.userId ) {

            await controller.saveFireIncident(data.userId);

            res.status(200).send({message:"success"});

        }else{

            res.status(422).send({message : "Invalid Request Parameter"})
        }

    }
    catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;