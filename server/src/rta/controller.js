import rtaRepo from "./rtaRepo";

module.exports = {

    getAllFines : async() => {

        return await rtaRepo.getAllFines();

    },

    getFineDetails : async(fineCode) => {

        return await rtaRepo.getScore(fineCode);

    },

    addFineToUserHistory : async (userId, fineDescription, fineCoins, location, time, fineSource, ethAddress, vehicleNo) => {

        let todaysDate = new Date();
        let today = todaysDate.toISOString().slice(0, 19).split('T')[0];

        await rtaRepo.addFineToHistory(userId,today,fineDescription,fineCoins,location,time,fineSource, ethAddress,vehicleNo);

    },

    subtractFineScoreFromUserMainScore : async(userId,fineScore) => {

        const presentUserTotalScore = await rtaRepo.getUserTotalScore(userId);

        let reducedScore = +(presentUserTotalScore) - +(fineScore);

        await rtaRepo.updateUserTotalScore(userId,reducedScore);

    },

    addHafilatToUserScoreHistory : async (userId,from,to,score,hafilattime) => {

        let todaysDate = new Date();
        let today = todaysDate.toISOString().slice(0, 19).split('T')[0];

        let scoreInString = "+"+score;

        await rtaRepo.addHafilatToHistory (userId,today,from,to,scoreInString,hafilattime);

    },

    addScoreToUserMainScore : async (userId,score) => {

        const presentUserTotalScore = await rtaRepo.getUserTotalScore(userId);

        let addedScore = +(presentUserTotalScore) + +(score);

        await rtaRepo.updateUserTotalScore(userId,addedScore);


    },

    getUserFineDetails : async (userId) => {

        const userFines = await rtaRepo.getUserFines(userId);

        let result = {};

        if (userFines.status) {

            result.vehicleNo = userFines.result[0].vehicleno;
            result.fines = userFines.result;

        } else {

            result.vehicleNo = "DXB I 87656";
            result.fines = [];

        }

        return result;
    },

    updateUserPaidFine : async (fineNo) => {

        await rtaRepo.updateFinePaidStatus(fineNo);

    }

}