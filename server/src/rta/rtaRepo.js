import DBService from "../services/dbservice";

module.exports = {

    getScore : async(code) => {

    let getScoreQuery = 'SELECT * FROM rtafines WHERE Id = ?';
    let getScoreQueryResult = await DBService.executeQuery({query: getScoreQuery, values: [code]});
  
    if (getScoreQueryResult.length >0 ) {
        return {fineCoins : getScoreQueryResult[0].finecoins, fineDescription : getScoreQueryResult[0].description};
    }

    },

    addFineToHistory : async(userId,today,fineDescription,fineCoins,location,time,fineSource,ethAddress,vehicleNo) => {

        return new Promise(async (resolve, reject) => {
            try {
                const values = [userId,today, fineDescription,location,time,fineSource,ethAddress,fineCoins,vehicleNo];
                const query_params = {
                    table: "userrtafines",
                    columns: ["userId", "date","rtatype","location","time","finesource","finesourceethereumaddress","fineincoins","vehicleno"],
                    values: [values]
                };
                await DBService.insertQuery(query_params);
                resolve();
            } catch (err) {
                reject(err);
            }
        });

    },

    getUserTotalScore : async (userId) => {

        let userScoreQuery = 'SELECT * FROM userscore WHERE userId = ?';
        let userScoreQueryResult = await DBService.executeQuery({query: userScoreQuery, values: [userId]});
  
        if (userScoreQueryResult.length > 0) {
            return userScoreQueryResult[0].totalscore;
        }

    },

    updateUserTotalScore :  async (userId, score) => {


        return new Promise(async (resolve, reject) => {
            try {
                const query_params = {
                    table: "userscore",
                    set: {
                        totalscore: score
                    },
                    where: {
                        userId: userId
                    }
                };
    
                await DBService.updateQuery(query_params);
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    },

    getAllFines : async() => {

    let query = 'SELECT * FROM rtafines';
    let result = await DBService.executeQuery({query: query});

    if (result.length > 0) {
        return result;
    }

   },

   addHafilatToHistory : async (userId,date,from,to,score,hafilattime) => {

    return new Promise(async (resolve, reject) => {
        try {
            const values = [userId,'rta', date, "publictransport",from,to,score,hafilattime];
            const query_params = {
                table: "userscoretransactions",
                columns: ["userId", "transactiontype", "date","rtatype","hafilatfrom","hafilatto","score","hafilattime"],
                values: [values]
            };
            await DBService.insertQuery(query_params);
            resolve();
        } catch (err) {
            reject(err);
        }
    });


   },

   getUserFines : async (userId) => {

    //let query = 'SELECT * FROM userrtafines WHERE userId = ? AND paymentstatus="false"';
    let query = `SELECT No,rtatype,location,time,finesource,finesourceethereumaddress,fineincoins,vehicleno  FROM userrtafines WHERE userId = '${userId}' AND paymentstatus="false"`;
    let result = await DBService.executeQuery({query: query});

    if (result.length > 0) {
        return {status:true,result:result};
    } else {
        return {status:false};
    }

   },

   updateFinePaidStatus : async (fineNo) => {


    return new Promise(async (resolve, reject) => {
        try {
            const query_params = {
                table: "userrtafines",
                set: {
                    paymentstatus: 'true'
                },
                where: {
                    No: fineNo
                }
            };

            await DBService.updateQuery(query_params);
            resolve();
        } catch (err) {
            reject(err);
        }
    });




   }

}