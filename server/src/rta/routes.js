import express from "express";
import controller from "./controller";
import marketPlaceController from "../marketplace/controller";
const router = express.Router();


router.post('/', async (req, res, next) => {

    try {

        const data = req.body;

        if (data.userId && data.fineCode && data.location && data.time && data.fineSource && data.fineSourceEthAddress && data.vehicleNo) {
  
            const {fineCoins, fineDescription } = await controller.getFineDetails(data.fineCode);
               
            await controller.addFineToUserHistory(data.userId,fineDescription,fineCoins,data.location,data.time, data.fineSource,data.fineSourceEthAddress, data.vehicleNo);
            
            res.status(200).send({message:"Success"});


        }else{

            res.status(422).send({message : "Invalid Request Parameter"})
        }
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.post('/hafilat', async (req, res, next) => {

    try {

       const data = req.body;

       console.log(data);

       let score = 100;

       if (data.userId && data.from && data.to && data.hafilattime) {
             
        //add it to userscore transaction (history);

        await controller.addHafilatToUserScoreHistory(data.userId,data.from,data.to,score,data.hafilattime);
           
        //add it from the main user score
        await controller.addScoreToUserMainScore(data.userId,score);

        //finally give success message
        res.status(200).send({message:"Success"});


       }else{

           res.status(422).send({message : "Invalid Request Parameter"})
       }
   }
   catch (e) {
       res.status(500).send(e);
   }
});


router.get('/', async (req, res, next) => {

    try {

       const data = req.query;

       if(data.fineCode) {

       const fineDetails = await controller.getFineDetails(data.fineCode);

       res.status(200).send(fineDetails);

       } else {

        const response = await controller.getAllFines();

        res.status(200).send(response);

       }
    }
    catch (e) {
        res.status(500).send(e);
    }
});


router.get('/fines', async (req, res, next) => {

    try {

       const data = req.query;

       if(data.userId) {

       const fineDetails = await controller.getUserFineDetails(data.userId);

       res.status(200).send(fineDetails);

    

       } else {

        res.status(200).send({message:"Invalid Query Parameter"});

       }
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.post('/payfine', async (req, res, next) => {

    try {

        const data = req.body;
           

        if (data.userId && data.password && data.No && data.fineSourceEthAddress && data.fineInCoins) {

            const userDetails = await marketPlaceController.checkUserHasCoins(data.userId, data.fineInCoins);
         
            console.log(userDetails);
            if (userDetails.status){
                //user has enough coins buy it

                const result = await marketPlaceController.sendCoinsToMerchant(userDetails.userAddress,data.fineInCoins,data.fineSourceEthAddress,data.password);

                if (result) {

                    await controller.updateUserPaidFine(data.No);

                    res.status(200).send({message:"You have successfuly paid the fine"});
                }


            } else {

                res.status(200).send({message: "You donot have enough coins to pay the fine"});

            }

        }else{

            res.status(422).send({message : "Invalid Input Parameter"})
        }
       

    }
    catch (e) {
        res.status(500).send(e);
    }
});






module.exports = router;