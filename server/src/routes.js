import express from 'express';
import PATH from 'path';
const router = express.Router();

const userRouter = require(PATH.resolve('src/user'));
router.use('/user', userRouter);

const walletRouter = require(PATH.resolve('src/wallet'));
router.use('/wallet', walletRouter);

const scoreRouter = require(PATH.resolve('src/score'));
router.use('/score', scoreRouter);

const stepRouter = require(PATH.resolve('src/steps'));
router.use('/steps', stepRouter);

const rtaRouter = require(PATH.resolve('src/rta'));
router.use('/rta', rtaRouter);

const coinRouter = require(PATH.resolve('src/coin'));
router.use('/coin', coinRouter);

const marketplaceRouter = require(PATH.resolve('src/marketplace'));
router.use('/marketplace', marketplaceRouter);

const energyUseRouter = require(PATH.resolve('src/energyuse'));
router.use('/energyuse', energyUseRouter);

const incidentReportRouter = require(PATH.resolve('src/incidentreport'));
router.use('/incidentreport', incidentReportRouter);

const socialVolunteerRouter = require(PATH.resolve('src/socialvolunteer'));
router.use('/socialvolunteer', socialVolunteerRouter);

const federalGovernmentRouter = require(PATH.resolve('src/federalgovernment'));
router.use('/federalgovernment', federalGovernmentRouter);

const socialWelfareRouter = require(PATH.resolve('src/socialwelfarefund'));
router.use('/socialwelfarefund', socialWelfareRouter);


const NotificationsRouter = require(PATH.resolve('src/notifications'));
router.use('/notifications', NotificationsRouter);

module.exports = router;