let fineSourceEthereumAddress;

function postHafilatUsage() {
	var source = $("#source").val()
    var destination = $("#destination").val();
    var hafilat_card = $("#hafilat_card").val();
    var date = $("#datetimepicker").data('date');

    $.ajax({
        url: 'http://115.114.36.146:2122/api/rta/hafilat',
        type: 'POST',
        dataType: "json",
        data: {
            "userId":27,
            "from":source,
            "to":destination,
            "hafilattime":date,
        },
        success: function(response, textStatus, jqXHR) {
          alert("Hafilat card usage updated successfully");
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert( "Oops!!! Something went wrong" );
        }
    });
 
}

function getCurrentTime() {
    var date = new Date();
    var prefix = date.toLocaleDateString();
    var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
    var am_pm = date.getHours() >= 12 ? "PM" : "AM";
    hours = hours < 10 ? "0" + hours : hours;
    var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
    time = prefix +" "+hours + ":" + minutes + " " +  am_pm;
    return time;
};

function postMawaqif() {
	var regstrtn_number = $("#regstrtn_number").val()
    var parking_offence = $("#parking_offence").val();
    var location = $("#location").val();
    let fineSource = $("#fine_source").val();
    // var date = $("#datetimepicker1").data('date');


    $.ajax({
         url:"http://115.114.36.146:2122/api/rta",
         method: "POST",
         data: {
            userId: 27,
            fineCode: parking_offence,
            location:location,
            time:getCurrentTime(),
            fineSource:fineSource,
            fineSourceEthAddress : fineSourceEthereumAddress,
            vehicleNo : regstrtn_number
         }   
    })
    .done(function() {
        alert( "Mawaqif details updated successfully" );
      })
      .fail(function() {
        alert( "Oops!!! Something went wrong" );
      });
}

function onParkingOffenceChange() {
    var selected_offence = $("#parking_offence").val();
    var amount = 0;
    offenceDetails.forEach(function (offence, i) {
        if(offence.Id == selected_offence) {
            amount = offence.finecoins;
        }
    });
    $("#fine_amount").val(amount)
}

function onFineSourceChange() {

    var selected_fine_source = $("#fine_source").val();

    if (selected_fine_source == "Dubai Police") {

        fineSourceEthereumAddress = "0x05e2980ba36c4f805da1811babf661b829ed9d2b";

    } else if (selected_fine_source == "Abu Dhabi Traffic Dept"){

        fineSourceEthereumAddress = "0x51fda29cbebae0e188899dfb435bcbe424a5c04f";

    }
}

var offenceDetails; 



$(function () {
    $("#demo-pie-2").attr("data-percent", "100");
    $.ajax({
        method: "GET", 
        url: "http://115.114.36.146:2122/api/rta"
    })
    .done(function(data) {
        offenceDetails = data;
        console.log("data",data);
        console.log("offence details",offenceDetails);

        if(data && data.length) {
            data.forEach(function (d, i) {
                $('#parking_offence')
                .append($("<option></option>")
                           .attr("value",d.Id)
                           .text(d.description));        
            });
        }
      })
      .fail(function(error) {
          console.log(error)
        alert( "Oops!!! Something went wrong" );
      });

      $.ajax({
        method: "GET", 
        url: "http://115.114.36.146:2122/api/federalgovernment"
    })
    .done(function(data) {
        console.log(data);
        $('#totalSupply').html("1 M");
        $('#withCustomer').html(data.userPosession);
        $('#withMerchant').html(data.merchantPosession);

        // let customerPercentage = (data.userPosession / 1000000) * 100;
        // let merchantPercentage = (data.merchantPosession / 1000000) * 100;

        let desiredArray = data.transactions[0];
        // console.log(desiredArray);

        desiredArray.forEach(function(value, key){

            
            let firstColumn = value.fromName;
                secondColumn = value.toName;
                thirdColumn = value.tokens;
                fourthColumn = value.time;

            var row = $('<tr/>').append($('<td/>', {text: firstColumn|| ''}))
                .append($('<td/>', {text: secondColumn|| ''}))
                .append($('<td/>', {text: thirdColumn || ''}))
                .append($('<td/>', {text: fourthColumn || ''}));

                $('table tbody').append(row);

        });

     })
      .fail(function(error) {
          console.log(error)
        alert( "Oops!!! Something went wrong" );
      });

      const fineSources = ["Dubai Police","Abu Dhabi Traffic Dept"]

      fineSources.forEach(function (d, i) {
        $('#fine_source')
        .append($("<option></option>")
                   .attr("value",d)
                   .text(d));        
    });

   
})