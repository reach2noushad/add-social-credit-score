-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: adda
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `marketplace`
--

DROP TABLE IF EXISTS `marketplace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace` (
  `productId` int(11) NOT NULL AUTO_INCREMENT,
  `productname` varchar(45) NOT NULL,
  `priceincoins` varchar(45) NOT NULL,
  `merchantname` varchar(45) NOT NULL,
  `merchantethereumaddress` varchar(45) DEFAULT NULL,
  `imageurl` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`productId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketplace`
--

LOCK TABLES `marketplace` WRITE;
/*!40000 ALTER TABLE `marketplace` DISABLE KEYS */;
INSERT INTO `marketplace` VALUES (1,'Amazon Echo Dot 3rd Gen','150','Lulu Hypermarket','0xfd8aB7dAf6a8D3e733A058b5efF0B6c9fcFe5B8c',NULL),(2,'Play Station 4','1000','Lulu Hypermarket','0xfd8aB7dAf6a8D3e733A058b5efF0B6c9fcFe5B8c',NULL),(3,'Diana Korr Handheld Bag','50','Lulu Hypermarket','0xfd8aB7dAf6a8D3e733A058b5efF0B6c9fcFe5B8c',NULL),(4,'Samsung Home Cinema','800','Lulu Hypermarket','0xfd8aB7dAf6a8D3e733A058b5efF0B6c9fcFe5B8c',NULL),(5,'Iphone XR','1200','Carrefour Hypermarket','0xa0056e9b86dF627Cb089d7E2B76AB8CC4804f516',NULL),(6,'Fossil - Gen 4 Smart Watch','80','Carrefour Hypermarket','0xa0056e9b86dF627Cb089d7E2B76AB8CC4804f516',NULL),(7,'Adidas Tshirt','20','Carrefour Hypermarket','0xa0056e9b86dF627Cb089d7E2B76AB8CC4804f516',NULL),(8,'Sony VA-190 Headset','50','Carrefour Hypermarket','0xa0056e9b86dF627Cb089d7E2B76AB8CC4804f516',NULL),(9,'Nikon D5600 DSLR Camera','1400','Carrefour Hypermarket','0xa0056e9b86dF627Cb089d7E2B76AB8CC4804f516',NULL);
/*!40000 ALTER TABLE `marketplace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rtafines`
--

DROP TABLE IF EXISTS `rtafines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rtafines` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `finecoins` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rtafines`
--

LOCK TABLES `rtafines` WRITE;
/*!40000 ALTER TABLE `rtafines` DISABLE KEYS */;
INSERT INTO `rtafines` VALUES (1,'Non-payment of parking tariff, or ticket is not visible','150'),(2,'Exceeding parking time','100'),(3,'Exceeding maximum parking hours','100'),(4,'Obstruction/Misuse of parking facility','200');
/*!40000 ALTER TABLE `rtafines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `emiratesId` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `mobileNumber` varchar(45) DEFAULT NULL,
  `ethereumAddress` varchar(45) DEFAULT NULL,
  `userNotificationStatus` varchar(45) DEFAULT 'false',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (20,'123456','Mohamed Noushad','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','00971501234567','58b02e608644479f33a5d595c99e584b7e5d0674','false'),(21,'987654321','Ahamed','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','00971521714687','9f13ab67b7ef43a74471f02d9cb3d57854669002','false'),(22,'147258369','Mohamed','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','00971507812724','b3e00131faa6e3f37ad53e4cfd128568cce887b3','false'),(23,'187258369','Ibrahim','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','00971505816112','4aed8d9a28ee9be1a3b0e56c2d1c991c2f1fde3e','false'),(24,'154875623','Ismail','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','00971522511986','7462f7d1288fb30595c0a9d7ab5009fa7b07a6ff','false'),(25,'14578452','Sajeer Hasan','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','00971553852064','1d389ede8515bf1b3965466b265a1c8ff5be8700','false'),(26,'2452435','Arshad Ahamed','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','00971509633331','f91480014884b52e48646a34cb2c0952924ff991','true');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userdevicetokens`
--

DROP TABLE IF EXISTS `userdevicetokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userdevicetokens` (
  `No` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(45) DEFAULT NULL,
  `devicetoken` text,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userdevicetokens`
--

LOCK TABLES `userdevicetokens` WRITE;
/*!40000 ALTER TABLE `userdevicetokens` DISABLE KEYS */;
INSERT INTO `userdevicetokens` VALUES (1,'25','asdjhsgdsvdahsdvsgvdvsvds'),(2,'25','asdjhsgdsvdahsdvsgvdvsvds'),(3,'25','asdjhsgdsvdahsdvsgvdvsvds'),(4,'25','cJuR6z2yMKM:APA91bHjuJpCrO2Eaw4d3JFXnRhXM6fTB_VEifxr6mnKyLkP5Jr9w3Qmsr2d7P4jAFsx6ZB3cWjowlfMko6gUUm2jRrizGAsPRHnJ8IyrDCngXXpZ6NcNK44Ck-tnXnzQhASWpQ-XY_y'),(5,'24','cJuR6z2yMKM:APA91bHjuJpCrO2Eaw4d3JFXnRhXM6fTB_VEifxr6mnKyLkP5Jr9w3Qmsr2d7P4jAFsx6ZB3cWjowlfMko6gUUm2jRrizGAsPRHnJ8IyrDCngXXpZ6NcNK44Ck-tnXnzQhASWpQ-XY_y'),(6,'25','cJuR6z2yMKM:APA91bHjuJpCrO2Eaw4d3JFXnRhXM6fTB_VEifxr6mnKyLkP5Jr9w3Qmsr2d7P4jAFsx6ZB3cWjowlfMko6gUUm2jRrizGAsPRHnJ8IyrDCngXXpZ6NcNK44Ck-tnXnzQhASWpQ-XY_y'),(7,'25','cJuR6z2yMKM:APA91bHjuJpCrO2Eaw4d3JFXnRhXM6fTB_VEifxr6mnKyLkP5Jr9w3Qmsr2d7P4jAFsx6ZB3cWjowlfMko6gUUm2jRrizGAsPRHnJ8IyrDCngXXpZ6NcNK44Ck-tnXnzQhASWpQ-XY_y'),(8,'25','c'),(9,'25','c'),(10,'26','cJuR6z2yMKM:APA91bHjuJpCrO2Eaw4d3JFXnRhXM6fTB_VEifxr6mnKyLkP5Jr9w3Qmsr2d7P4jAFsx6ZB3cWjowlfMko6gUUm2jRrizGAsPRHnJ8IyrDCngXXpZ6NcNK44Ck-tnXnzQhASWpQ-XY_y');
/*!40000 ALTER TABLE `userdevicetokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userrtafines`
--

DROP TABLE IF EXISTS `userrtafines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userrtafines` (
  `No` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `rtatype` text,
  `location` varchar(45) DEFAULT NULL,
  `time` varchar(45) DEFAULT NULL,
  `finesource` varchar(45) DEFAULT NULL,
  `finesourceethereumaddress` varchar(45) DEFAULT NULL,
  `fineincoins` varchar(45) DEFAULT NULL,
  `paymentstatus` varchar(45) DEFAULT 'false',
  `vehicleno` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userrtafines`
--

LOCK TABLES `userrtafines` WRITE;
/*!40000 ALTER TABLE `userrtafines` DISABLE KEYS */;
INSERT INTO `userrtafines` VALUES (6,23,'2019-06-18','Non-payment of parking tariff, or ticket is not visible','Al Muroor','06/18/2019 09:00 AM','Dubai Police','0x05E2980bA36c4F805Da1811bABf661b829eD9D2b','150','true','DXB I 87656'),(7,23,'2019-06-18','Non-payment of parking tariff, or ticket is not visible','Al Muroor','06/18/2019 09:00 AM','Dubai Police','0x05E2980bA36c4F805Da1811bABf661b829eD9D2b','150','false','DXB I 87656'),(8,26,'2019-07-03','Non-payment of parking tariff, or ticket is not visible','Al Muroor','06/18/2019 09:00 AM','Dubai Police','0x05E2980bA36c4F805Da1811bABf661b829eD9D2b','150','false','DXB I 123456'),(9,25,'2019-07-03','Non-payment of parking tariff, or ticket is not visible','Al Muroor','06/18/2019 09:00 AM','Dubai Police','0x05E2980bA36c4F805Da1811bABf661b829eD9D2b','150','false','DXB I 123456'),(10,25,'2019-07-03','Non-payment of parking tariff, or ticket is not visible','Al Muroor','06/18/2019 09:00 AM','Dubai Police','0x05E2980bA36c4F805Da1811bABf661b829eD9D2b','150','false','DXB I 123456');
/*!40000 ALTER TABLE `userrtafines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userscore`
--

DROP TABLE IF EXISTS `userscore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userscore` (
  `userId` int(11) NOT NULL,
  `usedscore` varchar(45) DEFAULT NULL,
  `totalscore` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userscore`
--

LOCK TABLES `userscore` WRITE;
/*!40000 ALTER TABLE `userscore` DISABLE KEYS */;
INSERT INTO `userscore` VALUES (20,'4000','6050'),(21,'1000','1740'),(22,'1000','1040'),(23,'1000','1655'),(25,'6000','6050'),(26,'6000','6220');
/*!40000 ALTER TABLE `userscore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userscoretransactions`
--

DROP TABLE IF EXISTS `userscoretransactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userscoretransactions` (
  `userId` int(11) NOT NULL,
  `transactiontype` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `stepcount` varchar(45) DEFAULT NULL,
  `rtatype` text,
  `energymetermonth` varchar(45) DEFAULT NULL,
  `energymeterusage` varchar(45) DEFAULT NULL,
  `incidenttype` varchar(45) DEFAULT NULL,
  `hafilatfrom` varchar(45) DEFAULT NULL,
  `hafilatto` varchar(45) DEFAULT NULL,
  `hafilattime` varchar(45) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `time` varchar(45) DEFAULT NULL,
  `socialvolunteertype` varchar(45) DEFAULT NULL,
  `score` varchar(45) DEFAULT NULL,
  `No` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userscoretransactions`
--

LOCK TABLES `userscoretransactions` WRITE;
/*!40000 ALTER TABLE `userscoretransactions` DISABLE KEYS */;
INSERT INTO `userscoretransactions` VALUES (20,'steps','2019-05-31','2633',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'303.3',1),(20,'steps','2019-06-03','3605',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'360.5',3),(20,'rta','2019-06-03',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,'Khalidiya','11:00 AM',NULL,'-150',5),(20,'rta','2019-06-03',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,'Electra','07:00 PM',NULL,'-150',6),(20,'rta','2019-06-03',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,'Muroor','08:30 PM',NULL,'-150',7),(20,'rta','2019-06-03',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,'Musaffah','09:00 PM',NULL,'-150',8),(20,'rta','2019-06-03',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,'Shahama','10:30 PM',NULL,'-150',9),(20,'rta','2019-06-03',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'-150',10),(20,'rta','2019-06-03',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'-150',11),(20,'rta','2019-06-06','','Non-payment of parking tariff, or ticket is not visible','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'-150',12),(20,'energyuse','2019-06-06',NULL,NULL,'5','700',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'+100',13),(20,'incidentreporting','2019-06-06',NULL,NULL,NULL,NULL,'fire',NULL,NULL,NULL,NULL,NULL,NULL,'+100',14),(20,'rta','2019-06-11',NULL,'Non-payment of parking tariff, or ticket is not visible','',NULL,NULL,'','',NULL,'Khalidiya','10:00 AM',NULL,'-150',15),(20,'steps','2019-06-11','2000',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'200',16),(20,'rta','2019-06-11',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,'Madina Zayed','2:30 PM','','-150',17),(20,'rta','2019-06-11',NULL,'publictransport','',NULL,'','Khalifa City','Masdar City',NULL,NULL,NULL,NULL,'+50',18),(20,'socialvolunteer','2019-06-11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'blooddonor','+100',19),(20,'socialvolunteer','2019-06-11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'charity','+100',20),(20,'energyuse','2019-05-05',NULL,'','4','800','',NULL,NULL,NULL,'','','','+0',21),(20,'incidentreporting','2019-06-11',NULL,NULL,NULL,NULL,'fire',NULL,NULL,NULL,NULL,NULL,NULL,'+100',22),(20,'incidentreporting','2019-06-11',NULL,NULL,NULL,NULL,'fire',NULL,NULL,NULL,NULL,NULL,NULL,'+100',23),(20,'rta','2019-06-12',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,'Madinat Zayed','02:30 PM',NULL,'-150',24),(20,'energyuse','2019-06-13',NULL,NULL,'1','14000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'+0',31),(20,'energyuse','2019-06-13',NULL,NULL,'2','12000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'+100',32),(20,'incidentreporting','2019-06-13',NULL,NULL,NULL,NULL,'fire',NULL,NULL,NULL,NULL,NULL,NULL,'+100',33),(20,'incidentreporting','2019-06-13',NULL,NULL,NULL,NULL,'fire',NULL,NULL,NULL,NULL,NULL,NULL,'+100',34),(20,'rta','2019-06-14',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,'Madina Zayed','02:45 PM',NULL,'-150',35),(20,'rta','2019-06-14',NULL,'publictransport',NULL,NULL,NULL,'Shahama','Khalidiya',NULL,NULL,NULL,NULL,'+150',36),(20,'rta','2019-06-14',NULL,'publictransport',NULL,NULL,NULL,'Shahama','Khalidiya',NULL,NULL,NULL,NULL,'+150',37),(20,'rta','2019-06-14',NULL,'publictransport',NULL,NULL,NULL,'Shahama','Khalidiya',NULL,NULL,NULL,NULL,'+150',38),(20,'rta','2019-06-14',NULL,'publictransport',NULL,NULL,NULL,'Shahama','Khalidiya',NULL,NULL,NULL,NULL,'+100',39),(20,'rta','2019-06-14',NULL,'publictransport',NULL,NULL,NULL,'Abu Dhabi','Al-Ain',NULL,NULL,NULL,NULL,'+100',40),(20,'rta','2019-06-14',NULL,'publictransport',NULL,NULL,NULL,'Abu Dhabi','Al-Ain',NULL,NULL,NULL,NULL,'+100',41),(20,'rta','2019-06-14',NULL,'publictransport',NULL,NULL,NULL,'Abu Dhabi','Al-Ain',NULL,NULL,NULL,NULL,'+100',42),(20,'rta','2019-06-14',NULL,'publictransport',NULL,NULL,NULL,'Abu Dhabi','Al-Ain','06/10/2019 2:36 PM',NULL,NULL,NULL,'+100',43),(20,'rta','2019-06-14',NULL,'publictransport',NULL,NULL,NULL,'Abu Dhabi','Al-Ain','06/10/2019 2:36 PM',NULL,NULL,NULL,'+100',44),(20,'rta','2019-06-14',NULL,'publictransport',NULL,NULL,NULL,'Hamdan St / Central Souq','St 25 / Al Mina Center','06/09/2019 3:30 PM',NULL,NULL,NULL,'+100',45),(20,'rta','2019-06-14',NULL,'Exceeding parking time',NULL,NULL,NULL,NULL,NULL,NULL,'Khhalidiya','1560506736852',NULL,'-100',46),(20,'rta','2019-06-14',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,'Muroor','03:41 PM',NULL,'-150',47),(20,'rta','2019-06-14',NULL,'Exceeding maximum parking hours',NULL,NULL,NULL,NULL,NULL,NULL,'Muroor','03:43 PM',NULL,'-100',48),(21,'steps','2019-06-15','10000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1000',49),(21,'rta','2019-06-15',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,'Madina Zayed','02:45 PM',NULL,'-150',50),(21,'rta','2019-06-15',NULL,'publictransport',NULL,NULL,NULL,'Shahama','Khalidiya','06/12/2019 2:27 PM',NULL,NULL,NULL,'+100',51),(21,'rta','2019-06-15',NULL,'publictransport',NULL,NULL,NULL,'Khalifa City','Masdar City','06/14/2019 3:30 PM',NULL,NULL,NULL,'+100',52),(21,'socialvolunteer','2019-06-15',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'blooddonor','+100',53),(21,'socialvolunteer','2019-06-15',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'charity','+100',54),(21,'energyuse','2019-06-15',NULL,NULL,'2','17000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'+0',55),(21,'energyuse','2019-06-15',NULL,NULL,'3','15000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'+100',56),(20,'incidentreporting','2019-06-15',NULL,NULL,NULL,NULL,'fire',NULL,NULL,NULL,NULL,NULL,NULL,'+100',57),(21,'incidentreporting','2019-06-15',NULL,NULL,NULL,NULL,'fire',NULL,NULL,NULL,NULL,NULL,NULL,'+100',58),(21,'steps','2019-06-16','1400',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'140',59),(21,'rta','2019-06-16',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,'Al Muroor','09:00 AM',NULL,'-150',60),(21,'rta','2019-06-16',NULL,'publictransport',NULL,NULL,NULL,'Electra','Khalidiya','06/15/2019 4:30 PM',NULL,NULL,NULL,'+100',61),(21,'socialvolunteer','2019-06-16',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'charity','+100',62),(21,'incidentreporting','2019-06-16',NULL,NULL,NULL,NULL,'fire',NULL,NULL,NULL,NULL,NULL,NULL,'+100',63),(22,'steps','2019-06-16','10400',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1040',64),(23,'steps','2019-06-16','14050',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1405',65),(23,'rta','2019-06-16',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,'Al Muroor','09:00 AM',NULL,'-150',66),(23,'rta','2019-06-16',NULL,'publictransport',NULL,NULL,NULL,'Electra','Khalidiya','06/15/2019 4:30 PM',NULL,NULL,NULL,'+100',67),(23,'rta','2019-06-16',NULL,'publictransport',NULL,NULL,NULL,'Electra','Khalidiya','06/14/2019 4:30 PM',NULL,NULL,NULL,'+100',68),(23,'incidentreporting','2019-06-16',NULL,NULL,NULL,NULL,'fire',NULL,NULL,NULL,NULL,NULL,NULL,'+100',69),(23,'incidentreporting','2019-06-16',NULL,NULL,NULL,NULL,'fire',NULL,NULL,NULL,NULL,NULL,NULL,'+100',70),(24,'rta','2019-06-18',NULL,'Non-payment of parking tariff, or ticket is not visible',NULL,NULL,NULL,NULL,NULL,NULL,'Al Muroor','09:00 AM',NULL,'-150',71),(25,'steps','2019-06-20','50500',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'5050',72),(24,'rta','2019-06-20',NULL,'publictransport',NULL,NULL,NULL,'Masdar City','Khalifa City','06/15/2019 2:15 PM',NULL,NULL,NULL,'+100',73),(25,'steps','2019-06-21','10000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1000',74),(26,'steps','2019-07-03','62200',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'6220',75);
/*!40000 ALTER TABLE `userscoretransactions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-03 15:49:19
